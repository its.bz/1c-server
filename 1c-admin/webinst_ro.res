  �'      ResB             [ P   �   n	  n	         n	  IDS_USAGE_WIN IDS_USAGE_LINUX IDS_UNKNOWN_PARAMETER IDS_IIS_NOTFOUND IDS_PATH_REQUIRED IDS_NAME_REQUIRED IDS_CONNSTR_REQUIRED IDS_CONFPATH_REQUIRED IDS_INCOMP_KEYS IDS_WEBSRV_REQUIRED IDS_WSDIR_OR_DESCRIPTOR_REQUIRED IDS_DIR_KEY_REQUIRED IDS_CONNSTR_OR_DESCRIPTOR_KEY_REQUIRED IDS_PUB_NOTFOUND IDS_WRONG_CONNECTIONSTRING IDS_WRONG_DIRECTORY IDS_WRONG_DIRECTORY1 IDS_DELETE_SUCCESS IDS_PUB_SUCCESS IDS_PUB_UPDATED IDS_OBSOLETTE_KEY IDS_EXCEPTION IDS_WWWROOT_DENIED IDS_WEBINST_NOADAPTER IDS_DESCRIPTOR_REQUIRED IDS_DESCRIPTORNOTFOUND IDS_ADMIN_PRIVILEGES_REQUIRED_WARNING_WINDOWS IDS_ADMIN_PRIVILEGES_REQUIRED_WARNING_LINUX �  E x c e p ci e :     P u b l i c a r e a   e s t e   r e � n o i t   N u   e s t e   g a s i t   d e s c r i p t o r u l   P u b l i c a r e a   e s t e   s a t i s f c u t   P u b l i c a ci a   n u   a   f o s t   g s i t   w e b s r v   -   p a r a m e t r u   o b l i g a t o r i u   W e b - s e r v e r u l   I I S     n - a   f o s t   g s i t .   C h e i l e   % s   _i   % s   s u n t   i n c o m p a t i b i l e .   C h e i a   % s   e s t e   � n v e c h i t ,   u t i l i z a ci   % s   L i s t a   % s   n u   p o a t e   f i   l i s t   d e   p u b l i c a r e   D e z a c t i v a r e a   p u b l i c r i i   e s t e   s a t i s f c u t   *�- d i r   -   p a r a m e t r u   o p ci o n a l   p e n t r u   p u b l i c a r e   /�N e c e s a r   s a   i n d i c a t i   c h e e a - w s d i r   s a u   - d e s c r i p t o r   /�P a r a m e t r u   n e c u n o s c u t   p e n t r u   r � n d u l   d e   c o m a n d :     0�N e c e s a r   i n d i c a r e a   c h e e i   - c o n n s t r   s a u   - d e s c r i p t o r   7�N e c e s a r   i n d i c a r e a   c a l e i   s p r e   . v r d   f i s i e r   i n   p a r a m e t r i :     8�T r e b u i e   s   s p e c i f i c a ci   n u m e l e   p u b l i c r i i   � n   p a r a m e t r u l :     :�D o s a r u l   d e s c r i p o t u l u i   n u   c o r e s p u n d e   c u   d o s a r u l   p u b l i c a t i e i   ;�T r e b u i e   s   s p e c i f i c a ci   c o m a n d a   d e   c o n e c t a r e   � n   p a r a m e t r u l :     =�F o l d e r u l   p a r a m e t r u l u i   - d i r   n u   c o i n c i d e   c u   f o l d e r u l   p u b l i c r i i   @�T r e b u i e   s   s p e c i f i c a ci   f o l d e r u l   p e n t r u   p u b l i c a r e   � n   p a r a m e t r u l :     N�T r e b u i e   s   s p e c i f i c a ci   c a l e a   c t r e   f i _i e r u l   c o n f   p e n t u r   A p a c h e   � n   p a r a m e t r u l :     d�C o m a n d a   d e   c o n e c t a r e   p e n t r u   p a r a m e t r u l   - c o n n s t r   n u   c o i n c i d e   c u   c o m a n d a   d e   c o n e c t a r e   p e n t r u   p u b l i c a r e   w�N u   s u n t   i n s t a l a t e   p l u g - i n - u r i   W e b - s e r v e r - u . 
 P e n t r u   e x e c u t a r e a   p u b l i c a i e i ,   t r e b u i e   s   m o d i f i c a ci   s e t a r e a   1 C : E n t e r p r i s e .   ��P e n t r u   e x e c u t a r e a   o p e r a i u n i i   d a t e   e s t e   n e v o i e   d e   d r e p t u r i l e   s u p e r u t i l i z a t o r u l u i   ( r o o t ) . 
 � n   c a z   c o n t r a r   e s t e   p o s i b i l   l u c r u l   i n c o r e c t   a   p r o g r a m e i . 
 P e n t r u   l a n s a r e a   � n   r e g i m u l   s u p e r u t i l i z a t o r u l u i   e x e c u t a i   c o m a n d a   p r i n   s u d o .   9�P e n t r u   e x e c u t a r e a   o p e r a i u n i i   d a t e   e s t e   n e c e s a r   d r e p t u r i l e   a d m i n i s t r a t o r u l u i   S O . 
 � n   c a z   c o n t r a t   e s t e   p o s i b i l   c a   p r o g r a m a   s   l u c r e z e   i n c o r e c t . 
 P e n t r u   l a n s a r e a   c u   d r e p t u r i l e   d e   a d m i n i s t r a t o r   e s t e   n e v o i e   s a   l a n s a i   r � n d u l   d e   c o m a n d   � n   r e g i m u l   a d m i n i s t r a t o r u l u i   i   a p o i   s   e x e c u t a i   c o m a n d a   � n   a c e s t   r � n d   d e   c o m a n d .   ���1 C : @54?@8OB85  8 .   #B8;8B0  ?C1;8:0F88  251- :;85=B0 
  
         C1;8:0F8O:  
  
         w e b i n s t   [ - p u b l i s h ]   w e b s r v   - w s d i r   V i r t u a l D i r   - d i r   D i r   - c o n n s t r   c o n n S t r   [ - c o n f P a t h   c o n f P a t h ]  
  
         C1;8:0F8O  =0  >A=>25  ACI5AB2CNI53>  v r d   D09;0:  
  
         w e b i n s t   [ - p u b l i s h ]   w e b s r v   [ - w s d i r   V i r t u a l D i r ]   - d e s c r i p t o r   v r d P a t h   - d i r   D i r   [ - c o n n s t r   c o n n S t r ]   [ - c o n f P a t h   c o n f P a t h ]  
  
         #40;5=85  ?C1;8:0F88:  
  
         w e b i n s t   - d e l e t e   w e b s r v   - w s d i r   V i r t u a l D i r   [ - d i r   D i r ]   [ - c o n n s t r   c o n n S t r ]   [ - c o n f P a t h   c o n f P a t h ]  
  
         #40;5=85  ?C1;8:0F88  ?>  ACI5AB2CNI5<C  v r d   D09;C:  
  
         w e b i n s t   - d e l e t e   w e b s r v   [ - w s d i r   V i r t u a l D i r ]   - d e s c r i p t o r   v r d P a t h   [ - d i r   D i r ]   [ - c o n n s t r   c o n n S t r ]   [ - c o n f P a t h   c o n f P a t h ]  
  
         ;NG8:  
  
                 - p u b l i s h :   >?C1;8:>20BL,   :;NG  ?>  C<>;G0=8N 
                 - d e l e t e :   C40;8BL  ?C1;8:0F8N 
                 w e b s r v  
                         - a p a c h e 2 :   ?C1;8:0F8O  51- :;85=B0  4;O  A p a c h e   2 . 0  
                         - a p a c h e 2 2 :   ?C1;8:0F8O  51- :;85=B0  4;O  A p a c h e   2 . 2  
                         - a p a c h e 2 4 :   ?C1;8:0F8O  51- :;85=B0  4;O  A p a c h e   2 . 4  
                 - w s d i r   V i r t u a l D i r :   28@BC0;L=K9  :0B0;>3 
                 - d i r   D i r :   D878G5A:89  :0B0;>3,   2  :>B>@K9  1C45B  >B>1@065=  28@BC0;L=K9 
                 - d e s c r i p t o r   v r d P a t h :   ?CBL  :  ACI5AB2CNI5<C  v r d   D09;C 
                 - c o n n s t r   c o n n S t r :   AB@>:0  A>548=5=8O   
                 - c o n f P a t h   c o n f P a t h :   ?>;=K9  ?CBL  :  :>=D83C@0F8>==><C  D09;C  A p a c h e   �ߣ1 C : @54?@8OB85  8 .   #B8;8B0  ?C1;8:0F88  251- :;85=B0 
  
         C1;8:0F8O:  
  
         w e b i n s t   [ - p u b l i s h ]   w e b s r v   - w s d i r   V i r t u a l D i r   - d i r   D i r   - c o n n s t r   c o n n S t r   [ - c o n f P a t h   c o n f P a t h ]   [ - o s a u t h ]  
  
         C1;8:0F8O  =0  >A=>25  ACI5AB2CNI53>  v r d   D09;0:  
  
         w e b i n s t   [ - p u b l i s h ]   w e b s r v   [ - w s d i r   V i r t u a l D i r ]   - d e s c r i p t o r   v r d P a t h   - d i r   D i r   [ - c o n n s t r   c o n n S t r ]   [ - c o n f P a t h   c o n f P a t h ]   [ - o s a u t h ]  
  
         #40;5=85  ?C1;8:0F88:  
  
         w e b i n s t   - d e l e t e   w e b s r v   - w s d i r   V i r t u a l D i r   [ - d i r   D i r ]   [ - c o n n s t r   c o n n S t r ]   [ - c o n f P a t h   c o n f P a t h ]  
  
         #40;5=85  ?C1;8:0F88  ?>  ACI5AB2CNI5<C  v r d   D09;C:  
  
         w e b i n s t   - d e l e t e   w e b s r v   [ - w s d i r   V i r t u a l D i r ]   - d e s c r i p t o r   v r d P a t h   [ - d i r   D i r ]   [ - c o n n s t r   c o n n S t r ]   [ - c o n f P a t h   c o n f P a t h ]  
  
         ;NG8:  
  
                 - p u b l i s h :   >?C1;8:>20BL,   :;NG  ?>  C<>;G0=8N 
                 - d e l e t e :   C40;8BL  ?C1;8:0F8N 
                 w e b s r v  
                         - i i s :   ?C1;8:0F8O  51- :;85=B0  4;O  I I S  
                         - a p a c h e 2 :   ?C1;8:0F8O  51- :;85=B0  4;O  A p a c h e   2 . 0  
                         - a p a c h e 2 2 :   ?C1;8:0F8O  51- :;85=B0  4;O  A p a c h e   2 . 2  
                         - a p a c h e 2 4 :   ?C1;8:0F8O  51- :;85=B0  4;O  A p a c h e   2 . 4  
                 - w s d i r   V i r t u a l D i r :   28@BC0;L=K9  :0B0;>3 
                 - d i r   D i r :   D878G5A:89  :0B0;>3,   2  :>B>@K9  1C45B  >B>1@065=  28@BC0;L=K9 
                 - d e s c r i p t o r   v r d P a t h :   ?CBL  :  ACI5AB2CNI5<C  v r d   D09;C 
                 - c o n n s t r   c o n n S t r :   AB@>:0  A>548=5=8O   
                 - c o n f P a t h   c o n f P a t h :   ?>;=K9  ?CBL  :  :>=D83C@0F8>==><C  D09;C  A p a c h e   ( B>;L:>  4;O  ?C1;8:0F88  =0  A p a c h e )  
                 - o s a u t h :   8A?>;L7>20=85  W i n d o w s   02B>@870F88  ( B>;L:>  4;O  ?C1;8:0F88  =0  I I S )    c5� � �� �T � w �e 5��> .   �� Fau� ������+% T � � M� ?\ @  ���7x � ��