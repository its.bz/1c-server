  �'      ResB             7     �  �  �  8       �  IDS_ERR_FILEINFOBASE_COMPONENT_MISSING IDS_ERR_UNKNOWNPARAMNAME IDS_ERR_CLIENT IDS_BY_ERROR_REASON IDS_PROHIBITED_APPID IDS_BAD_TICKET IDS_PROHIBITED_COMP81 IDS_ERR_EXTMD_PROCESSING_RIGHTS IDS_ERR_EXTMD_REPORT_RIGHTS IDS_ERR_DEBUGVRS_UNSPECIFIEDERROR IDS_ERR_DEBUGVRS_DEBUGGERNOTLOADED IDS_ERR_DEBUGVRS_EMODULEIDNOTRESOLVED IDS_ERR_DEBUGVRS_TARGETIDNOTSPECIFIED IDS_ERR_DEBUGVRS_INFOBASEIDNOTSPECIFIED IDS_ERR_DEBUGVRS_INFOBASENAMENOTSPECIFIED IDS_ERR_DEBUGVRS_MEASURERESULTSNOTSPECIFIED IDS_ERR_DEBUGVRS_MEASURESEANCEIDNOTSPECIFIED IDS_OPENID_ASSOC_FAIL IDS_HTTP_STATUS IDS_INVALID_OPENID_REQUEST IDS_OPENID_ERROR IDS_AUTH_ERROR IDS_OPENID_INVALID_PROVIDER_URL IDS_OPENID_INVALID_VALUE IDS_OPENID_HTTPS_CONNECTION_REQUIRED IDS_KEYSMNGR_LOCK_ERROR IDS_INVALID_POST_DATA IDS_INVALID_ASSOC_HANDLE IDS_DESCRIPTORDEBUGPROTOCOLERROR IDS_DESCRIPTORDEBUGPROTOCOLERROR_DETAIL IDS_OID2_RP_INVALID_AUTH IDS_OID2_USER_CANCEL IDS_OID2_PROTOCOL_ERR IDS_OID2_ASSOCIATIONS_NOT_SUPPORTED IDS_OID2_INVALID_OP_ENDPOINT IDS_OID2_INTERACTIVE_OP_AUTH IDS_OID2_ANOTHER_AUTH IDS_OID2_INVALID_CREDENTIALS IDS_OID2_RP_OP_CONNECTION_ERROR IDS_OID2_RP_RETURN_TO_LOGIN_QUESTION IDS_DBG_PROTOCOL_NOT_SPICIFIED IDS_DBG_PROTOCOL_DIFF_WITH_SERVER IDS_ERR_SEANCEREUSENOTAVAILABLE IDS_ERR_SEANCEREQUESTTIMOUT IDS_ERR_NOSEANCEID IDS_WEBSERVICENAME_NOT_DEFINED IDS_ERR_BAD_ALLOC IDS_OIDC_ACCESS_TOKEN_NOT_FOUND IDS_OIDC_CANT_GET_DISCOVERY_INFO IDS_OIDC_CANT_GET_EMAIL_FROM_USER_INFO IDS_OIDC_CANT_GET_USER_INFO_FROM_USER_ENDPOINT IDS_OIDC_IB_SET_USER_FAILED IDS_OIDC_IB_SET_USER_NOT_AUTHENTICATED IDS_OIDC_PROVIDER_CONFIG_NOT_FOUND IDS_OIDC_USER_ENDPOINT_CONF_NOT_FOUND openID2AuthFormT.html   
 r e a s o n : 
   R e t r y   l o g o n ?   O p e n I D   e r r o r   U n k n o w n   e r r o r   U n k n o w n   s e r v i c e   c l i e n t   a p p l i c a t i o n   D e b u g g e r   n o t   l o a d e d   U n k n o n w n   p r o v i d e r   U R L   O p e n I D   p r o t o c o l   e r r o r   U n k n o w n   p a r a m e t e r :   % s   I n v a l i d   O p e n I D   q u e r y .   A u t h e n t i c a t i o n   e r r o r .   S e r v e r   r e p l y   c o d e   % d .   E r r o r   l o c k i n g   k e y   m a n a g e r   I n v a l i d   d a t a   o f   P O S T   q u e r y   I n f o b a s e   I D   i s   n o t   s p e c i f i e d   C a n n o t   s t a r t   t h e   u s e r   s e s s i o n   S e s s i o n   r e u s e   i s   n o t   a v a i l a b l e   O p e n I D   a u t h e n t i c a t i o n   c a n c e l e d   I n f o b a s e   n a m e   i s   n o t   s p e c i f i e d   D e b u g   i t e m   I D   i s   n o t   s p e c i f i e d   O p e n I D   p r o v i d e r   n o t   a v a i l a b l e .   D e b u g   p r o t o c o l   i s   n o t   s p e c i f i e d   S e s s i o n   r e q u e s t   t i m e o u t   e x p i r e d   I n v a l i d   O p e n I D   p a r a m e t e r   v a l u e :     I n c o r r e c t   O p e n I D   p r o v i d e r   a d d r e s s   I n c o r r e c t   U R L   o f   O p e n I D   p r o v i d e r .   I n v a l i d   o r   m i s s i n g   u s e r   c r e d e n t i a l s   E r r o r   v a l i d a t i n g   O p e n I D   c r e d e n t i a l s   A n   O p e n I D   a u t h o r i z a t i o n   i s   d e t e c t e d   I n v a l i d   i n f o b a s e   u s e r   a u t h e n t i c a t i o n   G e n e r a t i o n   o f   m o d u l e   i d e n t i f i e r   f a i l e d   C a n n o t   r e t r i e v e   t o k e n   o r   p r o v i d e r   n a m e   E r r o r   c o n n e c t i n g   t o   t h e   O p e n I D   p r o v i d e r   )�U n k n o w n   v a l u e   o f   a s s o c _ h a n d l e   p a r a m e t e r :     +�O u t   o f   m e m o r y .   C a n n o t   p e r f o r m   t h e   o p e r a t i o n   +�C a n n o t   r e t r i e v e   u s e r   d a t a   f o r m   t h e   p r o v i d e r   /�P e r f o r m a n c e   m e a s u r e m e n t   r e s u l t   i s   n o t   s p e c i f i e d   1�E s t a b l i s h i n g   O p e n I D   a s s o c i a t i o n s   i s   n o t   s u p p o r t e d   2�M i s s i n g   r i g h t   f o r   i n t e r a c t i v e   e x t e r n a l   r e p o r t   l o a d   2�I n t e r a c t i v e   O p e n I D   a u t h e n t i c a t i o n   i s   n o t   s u p p o r t e d   3�P e r f o r m a n c e   m e a s u r e m e n t   s e s s i o n   I D   i s   n o t   s p e c i f i e d   4�E r r o r   l o a d i n g   f i l e - b a s e d   i n f o b a s e   s u p p o r t   c o m p o n e n t s   5�Y o u   h a v e   n o   r i g h t s   f o r   s t a r t i n g   a   c l i e n t   o f   t h i s   t y p e   8�T h e   O p e n I D   p r o v i d e r   r e p o r t e d   a   u s e r   a u t h e n t i c a t i o n   e r r o r   8�C a n n o t   i d e n t i f y   t h e   u s e r   e m a i l   f r o m   t h e   u s e r   i n f o r m a t i o n   9�I n v a l i d   U R L   o f   O p e n I D   p r o v i d e r :   r e q u i r e   H T T P S   c o n n e c t i o n .   ;�C a n n o t   r e t r i e v e   t h e   p r o v i d e r   c o n f i g u r a t i o n   f o r m   d e f a u l t . v r d   ;�T h e   d e b u g   p r o t o c o l   d o e s   n o t   m a t c h   t h e   s e r v e r   d e b u g   p r o t o c o l   ;�M i s s i n g   r i g h t   f o r   i n t e r a c t i v e   e x t e r n a l   d a t a   p r o c e s s o r s   l o a d   B�U n k n o w n   d e b u g   p r o t o c o l   s p e c i f i e d   f o r   t h e   a p p l i c a t i o n   b e i n g   s t a r t e d   F�D e b u g   p r o t o c o l   m i s m a t c h .   S e r v e r   p r o t o c o l :   % s ,   a p p l i c a t i o n   p r o t o c o l :   % s   G�C a n n o t   r e t r i e v e   p r o v i d e r   d a t a   t h r o u g h   o p e n i d   c o n n e c t   d i s c o v e r y   p r o t o c o l   G�S e s s i o n   m a n a g e m e n t   h e a d e r   o r   a   c o o k i e   w i t h   s e s s i o n   I D   i s   n o t   s p e c i f i e d .   _�
 C o m p a t i b i l i t y   w i t h   v e r s i o n   8 . 1   m o d e   i s   s e t . 
 S t a r t - u p   i n   t h e   m a n a g e d   a p p l i c a t i o n   m o d e   d i s a b l e d .   ����������;1  <!DOCTYPE HTML>
<HTML xmlns="http://www.w3.org/1999/xhtml" xmlns:v = "urn:schemas-microsoft-com:vml">
<HEAD>
    <META content="IE=edge" http-equiv="X-UA-Compatible">
    <META content="text/html; charset=utf-8" http-equiv="Content-Type">
    <META content="no" http-equiv="imagetoolbar">
    <META http-equiv='pragma' content='no-cache'/>
    <META http-equiv='cache-control' content='no-cache'/>
    <META http-equiv='expires' content='-1'/>
    <TITLE></TITLE>
    <STYLE type="text/css">
        BODY
        {
            color: #4d4d4d;
            background-color: #fff;
            margin: 0;
            padding: 0;
            border: 0;
            overflow: auto;
            font-family: 'MS Sans Serif', Arial, Verdana, sans-serif;
            font-size: 8pt;
            cursor: default;
            scrollbar-arrow-color: #908E8B;
            scrollbar-base-color: #F6F3DF;
            scrollbar-face-color: #F6F3DF;
            scrollbar-3dlight-color: #BDB79B;
            scrollbar-darkshadow-color: #CCC6AD;
            scrollbar-highlight-color: #F6F3DF;
            scrollbar-shadow-color: #F6F3DF;
            -webkit-tap-highlight-color: rgba(0,0,0,0);
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -webkit-text-size-adjust: none;
        }

        TABLE
        {
            font-family: Arial;
            font-size: 10pt;
        }

        INPUT
        {
            font-family: Arial;
            font-size: 10pt;
            -moz-user-select: text;
            -webkit-user-select: text;
            -webkit-text-size-adjust:none;
        }

        input:focus
        {
            outline-width: 0;
        }

        .noselect
        {
            -moz-user-select: none;
            -webkit-user-select: none;
        }

        .formCaption
        {
            text-align: left;
            vertical-align: middle;
            background-color: #E1DCD0;
            padding-left: 8px;
            font-size: 10pt;
            font-weight: bold;
        }

        .formHeader
        {
            border-bottom: 1px solid black;
            text-align: left;
            vertical-align: bottom;
        }

        .fieldLabel
        {
            padding-left: 28px;
            white-space: nowrap;
        }

        .checkBox
        {
            border: none;
            margin: 0;
        }

        .webButton
        {
            text-align: center;
            cursor: default;
            margin: 2px;
            padding: 0;
            -webkit-text-size-adjust:none;
            border-radius: 4px;
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            border: 1px solid #B3AC86;
            line-height: 18px;
            font-size: 10pt;
            font-family: Arial;
            background-color: #fafafa;
            color: #4d4d4d;
            filter: progid:DXImageTransform.Microsoft.gradient(gradientType='0', startColorstr='#FDFDFD', endColorstr='#EAEAEA'); /* для IE6-9 */
            background: linear-gradient(to bottom, #FDFDFD, #EAEAEA); /* для firefox 16+ */
            background: -moz-linear-gradient(top, #FDFDFD, #EAEAEA); /* для firefox 3.6+ */
            background: -ms-linear-gradient(top, #FDFDFD, #EAEAEA); /* для IE10+ */
            background: -webkit-gradient(linear, left top, left bottom, from(#FDFDFD), to(#EAEAEA));
        }

        .editBoxBorder
        {
            border-radius: 4px;
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            -webkit-text-size-adjust:none;
            position: relative;
            border: #A0A0A0 solid 1px;
            padding-left: 1px;
            padding-right: 1px;
        }

        .editBoxInput
        {
            width: 100%;
            height: 14px;
            padding: 1px;
            border-collapse: collapse;
            table-layout: fixed;
            background-color: white;
        }

        .authEditBox
        {
            border: none;
            background-color: white;
            width: 98%;
            margin-top: 0;
            margin-bottom: 0;
            padding-top: 1px;
            padding-bottom: 1px;
            line-height: 1.2;
            padding-left: 5px;
            font-family: Arial;
            font-size: 10pt;
        }

        #vert-hoz
        {
            position: absolute;
            top: 50%;
            left: 50%;
            margin-top: -80px;  /* half elements height*/
            margin-left: -180px;/* half elements width*/
            border: 1px solid silver;
            background: #666;
            overflow: auto;/* allow content to scroll inside element */
            text-align: left;
        }

        .openID2AuthForm
        {
            position: absolute;
            top: 50%;
            left: 50%;
            margin-top: -128px;  /* half elements height*/
            margin-left: -220px;/* half elements width*/
            width: 437px;
            height: 196px;
            background-color: #fff;
            overflow: hidden;
            border-left:solid 1px silver;
            border-top:solid 1px silver;
            border-right:solid 2px gray;
            border-bottom:solid 2px gray;
        }

        #userName, #userPassword
        {
            height: 21px;
            left: 1px;
            top: 1px;
        }

        #submit_btn
        {
            top: 10px;
            left: 0;
            height: 21px;
            width: 80px;
            line-height: 19px;
            padding-top: 0;
            padding-left: 0;
            border-color: rgb(191, 191, 191);
        }

        #cancel_btn
        {
            top: 10px;
            left: 88px;
            height: 21px;
            width: 80px;
            line-height: 19px;
            padding-top: 0;
            padding-left: 0;
            border-color: rgb(191, 191, 191);
        }
    </STYLE>
</HEAD>

<BODY style="overflow: hidden;">
<DIV id="authWindow" class="openID2AuthForm">
    <FORM style="height: 100%;" id="form" method="POST" action="<%OID2_ACTION_URL%>">
        <TABLE style="width: 100%; height: 100%; border-collapse: collapse;">
            <TBODY>
            <TR style="height: 24px;text-align:left;vertical-align:middle;background-color:#E1DCD0;padding-left:8px;font-size:10pt;font-weight:bold;">
                <TD class="noselect formCaption" colSpan="3"><%mngcore:IDS_WEB_1CENTERPRISE_TITLE%></TD>
            </TR>

            <TR>
                <TD style="width: 8px; padding-top: 2px;">&nbsp;</TD>
                <TD class="noselect formHeader"><%mngcore:IDS_WEB_ENTER_NAME_AND_PASSWORD%>:</TD>
                <TD style="width: 8px;">&nbsp;</TD>
            </TR>

            <TR>
                <TD style="width: 8px;">&nbsp;</TD>
                <TD style="vertical-align: middle;">
                    <TABLE style="width: 100%;">
                        <TBODY>
                        <TR>
                            <TD class="fieldLabel noselect"><%mngcore:IDS_WEB_USERNAME%>:</TD>
                            <TD style="width: 100%; padding-left: 8px;">
                                <DIV class="editBoxBorder">
                                    <TABLE class="editBoxInput">
                                        <TBODY>
                                        <TR>
                                            <TD style="width: auto; padding-left: 1px;">
                                                <INPUT id="userName" class="authEditBox" name="openid.auth.user" type="text" AUTOCOMPLETE="off" />
                                            </TD>
                                        </TR>
                                        </TBODY>
                                    </TABLE>
                                </DIV>
                            </TD>
                        </TR>

                        <TR>
                            <TD style="height: 1px;" colSpan="2"></TD>
                        </TR>

                        <TR>
                            <TD  class="fieldLabel noselect"><%mngcore:IDS_WEB_PASSWORD%>:</TD>
                            <TD style="padding-left: 8px;">
                                <DIV class="editBoxBorder">
                                    <TABLE class="editBoxInput">
                                        <TBODY>
                                        <TR>
                                            <TD style="width: auto; padding-left: 1px;">
                                                <INPUT id="userPassword" class="authEditBox" name="openid.auth.pwd" type="password" />
                                            </TD>
                                        </TR>
                                        </TBODY>
                                    </TABLE>
                                </DIV>
                            </TD>
                        </TR>

                        <TR>
                            <TD style="height: 1px;" colSpan="2"></TD>
                        </TR>

                        <TR>
                            <TD class="fieldLabel noselect"><%mngcore:IDS_WEB_SHORT_SESSION%>:</TD>
                            <TD style="padding-left: 8px;">
                                <TABLE>
                                    <TBODY>
                                    <TR>
                                        <TD style="width: auto; padding-left: 1px;">
                                            <INPUT class="checkBox" name='openid.auth.short' type="checkbox" value='true' />
                                        </TD>
                                    </TR>
                                    </TBODY>
                                </TABLE>
                            </TD>
                        </TR>
                        <TR>
                            <td>&nbsp;</td>
                            <TD style="height:35px;padding-left: 8px;">
                                <DIV style="height: 35px; float: left;">
                                    <INPUT id="submit_btn" class="webButton" value="<%mngbase:IDS_OK%>" type="submit"/>
                                    <INPUT id="cancel_btn" class="webButton" value="<%mngbase:IDS_CANCEL%>" type="button"/>
                                </DIV>
                            </TD>
                        </TR>
                        </TBODY>
                    </TABLE>
                </TD>
                <TD style="width: 8px;">&nbsp;</TD>
            </TR>
            </TR>

            </TBODY>
        </TABLE>

        <INPUT name="openid.return_to" value="<%OID2_RETURN_TO_VALUE%>" type="hidden"/>
        <INPUT name="openid.invalidate_handle" value="<%OID2_INVALIDATE_HANDLE_VALUE%>" type="hidden"/>

    </FORM>
</DIV>

<SCRIPT>
    var form      = document.getElementById("form");
    var btnCancel = document.getElementById("cancel_btn");
    btnCancel.onclick = function()
    {
        form.action = "<%OID2_CANCEL_URL%>";
        form.submit();
    }

    // Append styles for button pictures
    // https://URL_информационной_базы/e1cib/oid2op
    var MngsrvUrl = window.location.href.replace("e1cib/oid2op", "e1csys/mngsrv") + "/";
    if (MngsrvUrl[MngsrvUrl.length - 1] !== "/")
        MngsrvUrl += "/";

    function getImgBkgString(imgFileName)
    {
        return "    background-image: url(" + MngsrvUrl + imgFileName + "?sysver=<%V8VER%>);\n";
    }
    function getSelectorText(cssSelector, imgFileName)
    {
        return cssSelector + "\n{\n" + getImgBkgString(imgFileName) + "}\n";
    }
    function appendStyle(styles)
    {
        var css = document.createElement('style');
        css.type = 'text/css';

        if (css.styleSheet)
            css.styleSheet.cssText = styles;
        else
            css.appendChild(document.createTextNode(styles));

        document.getElementsByTagName("head")[0].appendChild(css);
    }

    var styles =
            getSelectorText(".webButton", "button.png") +
                    getSelectorText(".webButton:hover", "btnsel.png");

    window.onload = function()
    {
        //appendStyle(styles);
    };

</SCRIPT>
</BODY>
</HTML>
�8 �� o ��;\M` >���d� � �   ��G K"[�&�	<���Yy�_���9\5v���� � .����  `� `  `� `� `� `B `� `C  `V  `/ `# `� `* `� `� `%  `� `� `+ `� ` `^ `�  `�  `� `�  ` `�  `� `[ `� `� `Z `�  `� `} `  `} `V `� `� `� `@ `
 `G `j  `� `  ` `| `9 `a ` `3  `� 