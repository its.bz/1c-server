  �'      ResB             y     �      a       �  IDS_TYPE_PRESENTATION_PLANNERTYPE IDS_TYPE_PRESENTATION_PLANNERDIMENSIONTYPE IDS_TYPE_PRESENTATION_PLANNERDIMENSIONITEMTYPE IDS_TYPE_PRESENTATION_PLANNERITEMTYPE IDS_TYPE_PRESENTATION_PLANNERITEMSCHEDULETYPE IDS_TYPE_PRESENTATION_PLANNERITEMEXCEPTIONTYPE IDS_TYPE_PRESENTATION_PLANNERITEMCOLLECTIONTYPE IDS_TYPE_PRESENTATION_PLANNERDIMENSIONCOLLECTIONTYPE IDS_TYPE_PRESENTATION_PLANNERDIMENSIONITEMCOLLECTIONTYPE IDS_TYPE_PRESENTATION_PLANNERREPLACEMENTITEMCOLLECTIONTYPE IDS_TYPE_PRESENTATION_PLANNERCURRENTREPRESENTATIONPERIODCOLLECTIONTYPE IDS_TYPE_PRESENTATION_PLANNERREPRESENTATIONPERIODTYPE IDS_TYPE_PRESENTATION_PLANNERBACKGROUNDINTERVALTYPE IDS_TYPE_PRESENTATION_PLANNERBACKGROUNDINTERVALCOLLECTIONTYPE IDS_PLANNER_SCHEDULE_ENDDATE IDS_PLANNER_SCHEDULE_DAY_ONE IDS_PLANNER_SCHEDULE_WEEK_ONE IDS_PLANNER_SCHEDULE_MONTH_ONE IDS_PLANNER_SCHEDULE_YEAR_ONE IDS_PLANNER_SCHEDULE_EXECUTEWEEKDAYS IDS_PLANNER_SCHEDULE_EXECUTEMONTHES IDS_PLANNER_SCHEDULE_LASTNUMBER IDS_PLANNER_SCHEDULE_LASTMONTHDAY IDS_PLANNER_SCHEDULE_NUMREPEAT_ZERO IDS_PLANNER_SCHEDULE_NUMREPEAT_ONE IDS_PLANNER_SCHEDULE_NUMREPEAT_TWO IDS_PLANNER_SCHEDULE_NUMREPEAT_FEW IDS_PLANNER_SCHEDULE_NUMREPEAT_MANY IDS_PLANNER_SCHEDULE_NUMREPEAT_OTHER IDS_PLANNER_SCHEDULE_DAY_NUMS_ZERO IDS_PLANNER_SCHEDULE_DAY_NUMS_ONE IDS_PLANNER_SCHEDULE_DAY_NUMS_TWO IDS_PLANNER_SCHEDULE_DAY_NUMS_FEW IDS_PLANNER_SCHEDULE_DAY_NUMS_MANY IDS_PLANNER_SCHEDULE_DAY_NUMS_OTHER IDS_PLANNER_SCHEDULE_WEEK_NUMS_ZERO IDS_PLANNER_SCHEDULE_WEEK_NUMS_ONE IDS_PLANNER_SCHEDULE_WEEK_NUMS_TWO IDS_PLANNER_SCHEDULE_WEEK_NUMS_FEW IDS_PLANNER_SCHEDULE_WEEK_NUMS_MANY IDS_PLANNER_SCHEDULE_WEEK_NUMS_OTHER IDS_PLANNER_SCHEDULE_MONTH_NUMS_ZERO IDS_PLANNER_SCHEDULE_MONTH_NUMS_ONE IDS_PLANNER_SCHEDULE_MONTH_NUMS_TWO IDS_PLANNER_SCHEDULE_MONTH_NUMS_FEW IDS_PLANNER_SCHEDULE_MONTH_NUMS_MANY IDS_PLANNER_SCHEDULE_MONTH_NUMS_OTHER IDS_PLANNER_SCHEDULE_YEAR_NUMS_ZERO IDS_PLANNER_SCHEDULE_YEAR_NUMS_ONE IDS_PLANNER_SCHEDULE_YEAR_NUMS_TWO IDS_PLANNER_SCHEDULE_YEAR_NUMS_FEW IDS_PLANNER_SCHEDULE_YEAR_NUMS_MANY IDS_PLANNER_SCHEDULE_YEAR_NUMS_OTHER IDS_PLANNER_SCHEDULE_NUMBER_ZERO IDS_PLANNER_SCHEDULE_NUMBER_ONE IDS_PLANNER_SCHEDULE_NUMBER_TWO IDS_PLANNER_SCHEDULE_NUMBER_FEW IDS_PLANNER_SCHEDULE_NUMBER_MANY IDS_PLANNER_SCHEDULE_NUMBER_OTHER IDS_PLANNER_SCHEDULE_NEGATIVENUMBER_ZERO IDS_PLANNER_SCHEDULE_NEGATIVENUMBER_ONE IDS_PLANNER_SCHEDULE_NEGATIVENUMBER_TWO IDS_PLANNER_SCHEDULE_NEGATIVENUMBER_FEW IDS_PLANNER_SCHEDULE_NEGATIVENUMBER_MANY IDS_PLANNER_SCHEDULE_NEGATIVENUMBER_OTHER IDS_PLANNER_SCHEDULE_MONTHDAY_ZERO IDS_PLANNER_SCHEDULE_MONTHDAY_ONE IDS_PLANNER_SCHEDULE_MONTHDAY_TWO IDS_PLANNER_SCHEDULE_MONTHDAY_FEW IDS_PLANNER_SCHEDULE_MONTHDAY_MANY IDS_PLANNER_SCHEDULE_MONTHDAY_OTHER IDS_PLANNER_SCHEDULE_NEGATIVEMONTHDAY_ZERO IDS_PLANNER_SCHEDULE_NEGATIVEMONTHDAY_ONE IDS_PLANNER_SCHEDULE_NEGATIVEMONTHDAY_TWO IDS_PLANNER_SCHEDULE_NEGATIVEMONTHDAY_FEW IDS_PLANNER_SCHEDULE_NEGATIVEMONTHDAY_MANY IDS_PLANNER_SCHEDULE_NEGATIVEMONTHDAY_OTHER IDS_PLANNER_TEXT_COLOR IDS_PLANNER_BACK_COLOR IDS_PLANNER_BORDER_COLOR IDS_PLANNER_LINE_COLOR IDS_PLANNER_FONT IDS_PLANNER_BEGINOFREPRESENTATIONPERIOD IDS_PLANNER_ENDOFREPRESENTATIONPERIOD IDS_PLANNER_ALIGNELEMENTSOFTIMESCALE IDS_PLANNER_DISPLAYTIMESCALEWRAPHEADERS IDS_PLANNER_DISPLAYWRAPHEADERS IDS_PLANNER_DISPLAYCURRENTDATE IDS_PLANNER_PERIODICVARIANTUNIT IDS_PLANNER_PERIODICVARIANTREPETITION IDS_PLANNER_TIMESCALEWRAPBEGININDENT IDS_PLANNER_TIMESCALEWRAPENDINDENT IDS_DISPLAYELEMENTTIMEMODE_DONT_DISPLAY IDS_DISPLAYELEMENTTIMEMODE_DISPLAY_BEGIN_TIME IDS_DISPLAYELEMENTTIMEMODE_DISPLAY_BEGIN_END_TIME model.xdto plnnr.xsd   t o   N o n e   F o n t   P l a n n e r   % d   t i m e   % d   t i m e s   e v e r y   d a y   e v e r y   y e a r   e v e r y   w e e k   L i n e   c o l o r   T e x t   c o l o r   e v e r y   m o n t h   B o r d e r   c o l o r   P l a n n e r   i t e m   B e g i n n i n g   t i m e   e v e r y   % d n d   d a y   e v e r y   % d r d   d a y   e v e r y   % d t h   d a y   e v e r y   % d s t   d a y   e v e r y   % d s t   w e e k   e v e r y   % d t h   w e e k   e v e r y   % d r d   w e e k   e v e r y   % d n d   w e e k   e v e r y   % d n d   y e a r   e v e r y   % d r d   y e a r   e v e r y   % d t h   y e a r   e v e r y   % d s t   y e a r   T i m e   s c a l e   u n i t   s p e c i f i c   m o n t h s   B a c k g r o u n d   c o l o r   e v e r y   % d t h   m o n t h   e v e r y   % d s t   m o n t h   e v e r y   % d r d   m o n t h   e v e r y   % d n d   m o n t h   S h o w   c u r r e n t   d a t e   P l a n n e r   d i m e n s i o n   P l a n n e r   t i m e   r a n g e   S h o w   w r a p p e d   h e a d e r s   E x c l u d e d   p l a n n e r   i t e m   % d n d   d a y   o f   t h e   m o n t h   % d r d   d a y   o f   t h e   m o n t h   P l a n n e r   i t e m   s c h e d u l e   % d t h   d a y   o f   t h e   m o n t h   l a s t   d a y   o f   t h e   m o n t h   % d s t   d a y   o f   t h e   m o n t h   E n d   o f   t h e   t i m e   r a n g e   % d t h   w e e k   o f   t h e   m o n t h   % d r d   w e e k   o f   t h e   m o n t h   % d n d   w e e k   o f   t h e   m o n t h   B e g i n n i n g   a n d   e n d   t i m e   P l a n n e r   d i m e n s i o n   i t e m   l a s t   w e e k   o f   t h e   m o n t h   % d s t   w e e k   o f   t h e   m o n t h   P l a n n e r   i t e m   c o l l e c t i o n   s p e c i f i c   d a y s   o f   t h e   w e e k   T i m e   s c a l e   u n i t   m u l t i p l i e r   P l a n n e r   b a c k g r o u n d   i n t e r v a l   B e g i n n i n g   o f   t h e   t i m e   r a n g e   P l a n n e r   d i m e n s i o n   c o l l e c t i o n   % d t h   d a y   f r o m   t h e   e n d   o f   m o n t h   % d t h   w e e k   f r o m   t h e   e n d   o f   m o n t h   U p d a t e d   p l a n n e r   i t e m   c o l l e c t i o n   S h o w   w r a p p e d   t i m e   s c a l e   h e a d e r s   P l a n n e r   d i m e n s i o n   i t e m   c o l l e c t i o n   % d s t   d a y   f r o m   t h e   e n d   o f   t h e   m o n t h   % d n d   d a y   f r o m   t h e   e n d   o f   t h e   m o n t h   % d r d   d a y   f r o m   t h e   e n d   o f   t h e   m o n t h   % d n d   w e e k   f r o m   t h e   e n d   o f   t h e   m o n t h   % d r d   w e e k   f r o m   t h e   e n d   o f   t h e   m o n t h   % d s t   w e e k   f r o m   t h e   e n d   o f   t h e   m o n t h   C u r r e n t   p l a n n e r   t i m e   r a n g e   c o l l e c t i o n   P l a n n e r   b a c k g r o u n d   i n t e r v a l   c o l l e c t i o n   I n d e n t   i n   t h e   e n d   o f   w r a p p e d   t i m e   s c a l e   -�I n d e n t   i n   t h e   b e g i n n i n g   o f   w r a p p e d   t i m e   s c a l e   -�A l i g n   i t e m   b o u n d a r i e s   w i t h   t i m e   s c a l e   b o r d e r s   �������  ﻿<model xmlns="http://v8.1c.ru/8.1/xdto" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<package targetNamespace="http://v8.1c.ru/8.3/data/planner" elementFormQualified="true" attributeFormQualified="false">
		<import namespace="http://v8.1c.ru/8.1/data/core"/>
		<import namespace="http://v8.1c.ru/8.1/data/ui"/>
		<import namespace="http://v8.1c.ru/8.2/data/chart"/>
		<valueType name="PlannerItemScheduleMonths" itemType="xs:decimal"/>
		<valueType name="PlannerItemScheduleWeekDays" itemType="xs:decimal"/>
		<valueType name="PlannerItemsTimeRepresentation" base="xs:string">
			<enumeration>DontDisplay</enumeration>
			<enumeration>BeginTime</enumeration>
			<enumeration>BeginAndEndTime</enumeration>
		</valueType>
		<objectType name="Planner">
			<property xmlns:d4p1="http://v8.1c.ru/8.3/data/planner" name="item" type="d4p1:PlannerItem" lowerBound="0" upperBound="-1"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.3/data/planner" name="dimension" type="d4p1:PlannerDimension" lowerBound="0" upperBound="-1"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.3/data/planner" name="period" type="d4p1:PlannerRepresentationPeriod" lowerBound="0" upperBound="-1"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.3/data/planner" name="backInterval" type="d4p1:PlannerBackgroundInterval" lowerBound="0" upperBound="-1"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="borderColor" type="d4p1:Color"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="textColor" type="d4p1:Color"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="backColor" type="d4p1:Color"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="lineColor" type="d4p1:Color"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="font" type="d4p1:Font"/>
			<property name="beginOfRepresentationPeriod" type="xs:dateTime"/>
			<property name="endOfRepresentationPeriod" type="xs:dateTime"/>
			<property name="alignElementsOfTimeScale" type="xs:boolean" lowerBound="0" default="false"/>
			<property name="displayTimeScaleWrapHeaders" type="xs:boolean" lowerBound="0" default="true"/>
			<property name="displayWrapHeaders" type="xs:boolean" lowerBound="0" default="true"/>
			<property name="displayCurrentDate" type="xs:boolean" lowerBound="0" default="true"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/core" name="timeScaleWrapHeadersFormat" type="d4p1:LocalStringType"/>
			<property name="periodicVariantRepetition" type="xs:decimal"/>
			<property name="timeScaleWrapBeginIndent" type="xs:decimal"/>
			<property name="timeScaleWrapEndIndent" type="xs:decimal"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/chart" name="timeScale" type="d4p1:TimeScale"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/chart" name="periodicVariantUnit" type="d4p1:TimeScaleUnitType"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.3/data/planner" name="itemsTimeRepresentation" type="d4p1:PlannerItemsTimeRepresentation"/>
		</objectType>
		<objectType name="PlannerBackgroundInterval">
			<property name="begin" type="xs:dateTime"/>
			<property name="end" type="xs:dateTime"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="color" type="d4p1:Color"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/core" name="dimensionValues" type="d4p1:FixedMap" lowerBound="0" nillable="true"/>
		</objectType>
		<objectType name="PlannerDimension">
			<property xmlns:d4p1="http://v8.1c.ru/8.3/data/planner" name="item" type="d4p1:PlannerDimensionItem" lowerBound="0" upperBound="-1"/>
			<property name="value" nillable="true"/>
			<property name="text" type="xs:string"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="borderColor" type="d4p1:Color"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="textColor" type="d4p1:Color"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="backColor" type="d4p1:Color"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="font" type="d4p1:Font"/>
		</objectType>
		<objectType name="PlannerDimensionItem">
			<property name="value" lowerBound="0" nillable="true"/>
			<property name="text" type="xs:string"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="borderColor" type="d4p1:Color"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="textColor" type="d4p1:Color"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="backColor" type="d4p1:Color"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="font" type="d4p1:Font"/>
		</objectType>
		<objectType name="PlannerItem">
			<property name="value" lowerBound="0" nillable="true"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="picture" type="d4p1:Picture"/>
			<property name="begin" type="xs:dateTime"/>
			<property name="end" type="xs:dateTime"/>
			<property name="text" type="xs:string"/>
			<property name="tooltip" type="xs:string"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="borderColor" type="d4p1:Color"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="textColor" type="d4p1:Color"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="backColor" type="d4p1:Color"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="font" type="d4p1:Font"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.3/data/planner" name="schedule" type="d4p1:PlannerItemSchedule" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/core" name="dimensionValues" type="d4p1:FixedMap"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.3/data/planner" name="replacementItem" type="d4p1:PlannerItem" lowerBound="0" upperBound="-1"/>
			<property name="replacementDate" type="xs:dateTime" nillable="true"/>
			<property name="deleted" type="xs:boolean" nillable="true"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/core" name="id" type="d4p1:UUID"/>
		</objectType>
		<objectType name="PlannerItemSchedule">
			<property name="endDate" type="xs:dateTime"/>
			<property name="dayInMonth" type="xs:decimal" lowerBound="0" default="0"/>
			<property name="weekDayInMonth" type="xs:decimal" lowerBound="0" default="0"/>
			<property name="daysRepeat" type="xs:decimal" lowerBound="0" default="1"/>
			<property name="weeksRepeat" type="xs:decimal" lowerBound="0" default="0"/>
			<property name="monthsRepeat" type="xs:decimal" lowerBound="0" default="0"/>
			<property name="yearsRepeat" type="xs:decimal" lowerBound="0" default="0"/>
			<property name="repeatCount" type="xs:decimal" lowerBound="0" default="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.3/data/planner" name="weekDays" type="d4p1:PlannerItemScheduleWeekDays"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.3/data/planner" name="months" type="d4p1:PlannerItemScheduleMonths"/>
		</objectType>
		<objectType name="PlannerRepresentationPeriod">
			<property name="begin" type="xs:dateTime"/>
			<property name="end" type="xs:dateTime"/>
		</objectType>
	</package>
</model>�������������  ﻿<?xml version="1.0" encoding="UTF-8" ?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           xmlns:ui="http://v8.1c.ru/8.1/data/ui"
           xmlns:core="http://v8.1c.ru/8.1/data/core"
           xmlns:tns="http://v8.1c.ru/8.3/data/planner"
           xmlns:chart="http://v8.1c.ru/8.2/data/chart"
           targetNamespace="http://v8.1c.ru/8.3/data/planner"
           elementFormDefault="qualified"
           attributeFormDefault="unqualified"
           version="1.0">

    <xs:import namespace="http://v8.1c.ru/8.1/data/core" schemaLocation="../../../xdto/src/res/core.xsd"/>
    <xs:import namespace="http://v8.1c.ru/8.1/data/ui" schemaLocation="../../../xdto/src/res/uiobjects.xsd"/>
    <xs:import namespace="http://v8.1c.ru/8.1/data/ui" schemaLocation="../../../xdto/src/res/uiobjects.xsd"/>
    <xs:import namespace="http://v8.1c.ru/8.2/data/chart" schemaLocation="../../../chart/src/res/chart.xsd"/>
    
    <xs:simpleType name="PlannerItemScheduleWeekDays">
        <xs:list itemType="xs:decimal" />
    </xs:simpleType>

    <xs:simpleType name="PlannerItemScheduleMonths" >
        <xs:list itemType="xs:decimal" />
    </xs:simpleType>

    <!-- Режим отображения времени на элементе -->
    <xs:simpleType name="PlannerItemsTimeRepresentation">
        <xs:restriction base="xs:string">
            <xs:enumeration value="DontDisplay" />
            <xs:enumeration value="BeginTime" />
            <xs:enumeration value="BeginAndEndTime" />
        </xs:restriction>
    </xs:simpleType>

    <!-- Данные планировщика -->
    <xs:complexType name="Planner">
        <xs:sequence>
            <xs:element name="item" type="tns:PlannerItem" minOccurs="0" maxOccurs="unbounded" />
            <xs:element name="dimension" type="tns:PlannerDimension" minOccurs="0" maxOccurs="unbounded" />
            <xs:element name="period" type="tns:PlannerRepresentationPeriod" minOccurs="0" maxOccurs="unbounded" />
            <xs:element name="backInterval" type="tns:PlannerBackgroundInterval" minOccurs="0" maxOccurs="unbounded" />
            <xs:element name="borderColor" type="ui:Color" />
            <xs:element name="textColor" type="ui:Color" />
            <xs:element name="backColor" type="ui:Color" />
            <xs:element name="lineColor" type="ui:Color" />
            <xs:element name="font" type="ui:Font" />
            <xs:element name="beginOfRepresentationPeriod" type="xs:dateTime" />            
            <xs:element name="endOfRepresentationPeriod" type="xs:dateTime" />
            <xs:element name="alignElementsOfTimeScale" type="xs:boolean" default="false" minOccurs="0" />
            <xs:element name="displayTimeScaleWrapHeaders" type="xs:boolean" default="true" minOccurs="0" />
            <xs:element name="displayWrapHeaders" type="xs:boolean" default="true" minOccurs="0" />
            <xs:element name="displayCurrentDate" type="xs:boolean" default="true" minOccurs="0" />
            <xs:element name="timeScaleWrapHeadersFormat" type="core:LocalStringType" />
            <xs:element name="periodicVariantRepetition" type="xs:decimal" />
            <xs:element name="timeScaleWrapBeginIndent" type="xs:decimal" />
            <xs:element name="timeScaleWrapEndIndent" type="xs:decimal" />
            <xs:element name="timeScale" type="chart:TimeScale" />
            <xs:element name="periodicVariantUnit" type="chart:TimeScaleUnitType" />
            <xs:element name="itemsTimeRepresentation" type="tns:PlannerItemsTimeRepresentation" />
        </xs:sequence>
    </xs:complexType>
    
    <!-- Измерение планировщика -->
    <xs:complexType name="PlannerDimension">
        <xs:sequence>
            <xs:element name="item" type="tns:PlannerDimensionItem" minOccurs="0" maxOccurs="unbounded" />
            <xs:element name="value" type="xs:anyType" nillable="true" />
            <xs:element name="text" type="xs:string" />
            <xs:element name="borderColor" type="ui:Color" />
            <xs:element name="textColor" type="ui:Color" />
            <xs:element name="backColor" type="ui:Color" />
            <xs:element name="font" type="ui:Font" />
        </xs:sequence>
    </xs:complexType>
    
    <!-- Элемент планировщика -->
    <xs:complexType name="PlannerItem">
        <xs:sequence>
            <xs:element name="value" type="xs:anyType" minOccurs="0" nillable="true" />
            <xs:element name="picture" type="ui:Picture" />
            <xs:element name="begin" type="xs:dateTime" />
            <xs:element name="end" type="xs:dateTime" />
            <xs:element name="text" type="xs:string" />
            <xs:element name="tooltip" type="xs:string" />
            <xs:element name="borderColor" type="ui:Color" />
            <xs:element name="textColor" type="ui:Color" />
            <xs:element name="backColor" type="ui:Color" />
            <xs:element name="font" type="ui:Font" />
            <xs:element name="schedule" type="tns:PlannerItemSchedule" minOccurs="0" />
            <xs:element name="dimensionValues" type="core:FixedMap" />
            <xs:element name="replacementItem" type="tns:PlannerItem" minOccurs="0" maxOccurs="unbounded" />
            <xs:element name="replacementDate" type="xs:dateTime" nillable="true" />
            <xs:element name="deleted" type="xs:boolean" nillable="true" />
            <xs:element name="id" type="core:UUID" />
        </xs:sequence>
    </xs:complexType>

    <!-- Элемент измерения планировщика -->
    <xs:complexType name="PlannerDimensionItem">
        <xs:sequence>
            <xs:element name="value" type="xs:anyType" minOccurs="0" nillable="true" />
            <xs:element name="text" type="xs:string" />
            <xs:element name="borderColor" type="ui:Color" />
            <xs:element name="textColor" type="ui:Color" />
            <xs:element name="backColor" type="ui:Color" />
            <xs:element name="font" type="ui:Font" />
        </xs:sequence>
    </xs:complexType>
    
    <!-- Расписание элемента планировщика -->
    <xs:complexType name="PlannerItemSchedule">
        <xs:sequence>
            <xs:element name="endDate" type="xs:dateTime"/>
            <xs:element name="dayInMonth" type="xs:decimal" default="0" minOccurs="0" />
            <xs:element name="weekDayInMonth" type="xs:decimal" default="0" minOccurs="0" />
            <xs:element name="daysRepeat" type="xs:decimal" default="1" minOccurs="0" />
            <xs:element name="weeksRepeat" type="xs:decimal" default="0" minOccurs="0" />
            <xs:element name="monthsRepeat" type="xs:decimal" default="0" minOccurs="0" />
            <xs:element name="yearsRepeat" type="xs:decimal" default="0" minOccurs="0" />
            <xs:element name="repeatCount" type="xs:decimal" default="0" minOccurs="0" />
            <xs:element name="weekDays" type="tns:PlannerItemScheduleWeekDays" />
            <xs:element name="months" type="tns:PlannerItemScheduleMonths" />
        </xs:sequence>
    </xs:complexType>

    <!-- Период отображения планировщика -->
    <xs:complexType name="PlannerRepresentationPeriod">
        <xs:sequence>
            <xs:element name="begin" type="xs:dateTime" />
            <xs:element name="end" type="xs:dateTime" />
        </xs:sequence>
    </xs:complexType>

    <!-- Интервал фона планировщика -->
    <xs:complexType name="PlannerBackgroundInterval">
        <xs:sequence>
            <xs:element name="begin" type="xs:dateTime" />
            <xs:element name="end" type="xs:dateTime" />
            <xs:element name="color" type="ui:Color" />
            <xs:element name="dimensionValues" type="core:FixedMap" minOccurs="0" nillable="true" />
        </xs:sequence>
    </xs:complexType>
</xs:schema>

���a ���z�,����T%Df �"���|��T
v

�
2
�	��9��?<f�
��
r	�	"	�	J	���V�v5q�+�N:�^��!����_^�Kp�u�O�m B � � � �?  & `�  `  ` `n `� `i  `� `a `� `� `	  `G  `� `N `�  `    �  `�  `�  `    '  `  `^ `x `2 `} `� `    I `� `� `    � `    � ` `� `    ]  `0 `    T `! ` `    � `    � ` `� `    ; `    � `g `% `              `  `        �  `    �  `�  `�  `    <  ` `    > `. ` `    1  `R  `� `� `� `� `x `� `� ` `� `` ` `Q `v  `A `� `  `� � 