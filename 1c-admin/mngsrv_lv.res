  �'      ResB             7)     C  #*  #*  �         IDS_SETPROP_SESSIONPARAMSETTINGS_ONLY IDS_EDITBTNTOOLTIP_CLEAR IDS_EDITBTNTOOLTIP_LISTSEL IDS_EDITBTNTOOLTIP_SPECSEL IDS_EDITBTNTOOLTIP_OPEN IDS_EDITBTNTOOLTIP_UP IDS_EDITBTNTOOLTIP_DOWN IDS_EDITBTNTOOLTIP_DROPLIST IDS_ADDIN_TITLE IDS_FSE_TITLE IDS_CPE_TITLE IDS_AGENT_TITLE IDS_CRYPTOEXTENSION IDS_HTML_COPYRIGHT IDS_HTML_RUN_MODE_DESCR IDS_HTML_EXTERN_MODULE_INSTALL_IN_PROGRESS IDS_HTML_EXTERN_MODULE_INSTALLED IDS_HTML_EXTERN_MODULE_ALREADY_INSTALLED IDS_HTML_EXTERN_MODULE_INSTALL_FAILED IDS_CONFIRM_INSTALL_IE IDS_VERIFY_PUBLISHER IDS_INSTALL_NOW_IE IDS_PRESS_CONTINUE IDS_FILE_EXT_INSTALL IDS_CRYPTO_EXT_INSTALL IDS_AGENT_EXT_INSTALL IDS_EXT_MODULE_INSTALL IDS_CHROME_EXT_INSTALL_AGREE_MSG IDS_CHROME_EXT_INSTALL_REJECT_MSG IDS_SAFARI_EXT_INSTALL_TIP_MACOS IDS_SAFARI_EXT_INSTALL_DOWNLOAD IDS_CHROME_37_EXT_INSTALL_BEGIN IDS_CHROME_37_EXT_INSTALL_FILES_EXT IDS_CHROME_37_EXT_INSTALL_CRYPTO_EXT IDS_CHROME_37_EXT_INSTALL_AGENT_EXT IDS_CHROME_37_EXT_INSTALL_ADDIN_EXT IDS_CHROME_37_EXT_INSTALL_WEB_EXT IDS_CHROME_37_EXT_INSTALL_WEB_EXT_LIN IDS_CHROME_37_EXT_INSTALL_HOST_EXT IDS_CHROME_37_EXT_INSTALL_HOST_EXT_LIN IDS_CHROME_37_EXT_INSTALL_HOST_EXT_LEFT IDS_CHROME_37_EXT_INSTALL_HOST_EXT_LEFT_LIN IDS_CHROME_37_EXT_INSTALL_NEED_RESTART IDS_CHROME_37_EXT_INSTALL_WAIT IDS_CHROME_37_EXT_INSTALL_WAIT_LIN IDS_CHROME_37_EXT_INSTALL_RESTART IDS_EDGE_START_IN_IE11 IDS_CHROME_CLIPBOARD_EXT_NOT_INSTALLED IDS_CHROME_CLIPBOARD_EXT_NEED_RESTART IDS_CHROME_EXT_ATTEMPT_TO_USE IDS_CHROME_EXT_INSTALL_MAYBE_LATER IDS_FF_EXT_INSTALL_WEB_EXT IDS_FF_EXT_INSTALL_HOST_EXT_LEFT IDS_FF_EXT_INSTALL_HOST_EXT_LEFT_LIN IDS_FF_CLIPBOARD_EXT_NOT_INSTALLED IDS_HTML_BROWSER_SETTINGS_INFO_CONFIRMATION IDS_HTML_COLOR_CHOOSE IDS_HTML_RED IDS_HTML_GREEN IDS_HTML_BLUE IDS_HTML_BACKGOUND_COLOR_PREVIEW IDS_HTML_TEXT_COLOR_PREVIEW IDS_HTML_OPEN_CALCULATOR IDS_HTML_OPEN_CALENDAR IDS_HTML_COPY_TO_CLIPBOARD_AS_NUMBER IDS_HTML_ADD_NUMBER_TO_CLIPBOARD IDS_HTML_SUBTRACT_NUMBER_FROM_CLIPBOARD IDS_HTML_SERVICE IDS_HTML_FORM_CUSTOMIZATION IDS_HTML_TOOLBAR_CUSTOMIZE IDS_HTML_ADD_REMOVE_BUTTONS IDS_HTML_ADD_GROUP IDS_HTML_ADD_FIELDS IDS_HTML_DELETE_CURRENT_ITEM IDS_HTML_MOVE_CURRENT_ITEM_UP IDS_HTML_MOVE_CURRENT_ITEM_DOWN IDS_HTML_MARK_ALL IDS_HTML_UNMARK_ALL IDS_HTML_DELETE IDS_HTML_MOVE_UP IDS_HTML_MOVE_DOWN IDS_HTML_RESTORE_DEFAULT_SETTINGS IDS_HTML_FORM_ITEM_PROPERTIES IDS_HTML_CHOOSE_FIELDS_TO_PLACE_ON_FORM IDS_HTML_LOADING IDS_HTML_THEMES_PANEL IDS_HTML_NAVIGATION_PANEL IDS_HTML_NAVIGATION2_PANEL IDS_HTML_OPEN_HELP IDS_HTML_SHOW_ABOUT IDS_HTML_FAVORITE_LINKS IDS_HTML_FAVORITES IDS_HTML_HISTORY IDS_HTML_ENTER_NAME_AND_PASSWORD IDS_HTML_EXIT IDS_HTML_REPEAT_ENTER IDS_HTML_FILE_UPLOADING IDS_HTML_FILE IDS_HTML_UPLOADING IDS_HTML_APPLY IDS_HTML_SHOW_BUTTON IDS_HTML_HISTORY_TITLE IDS_HTML_TOPICSELECTOR_TITLE IDS_HTML_FILE_DOWNLOADING IDS_RTE_ACTIVEDOCUMENTGET_ERROR IDS_RTE_CONNECTION_UNEXPECTEDLY_CLOSED IDS_RESET_TOOLBAR IDS_HTML_CLIPSTORE IDS_HTML_CLIPPLUS IDS_HTML_CLIPMINUS IDS_HTML_WINDOW_ACTIVATED_MSG IDS_HTML_UNABLE_TO_SWITCH_TO_MAINWINDOW IDS_HTML_CLIPBOARD_INACCESSIBLE IDS_HTML_USE_BROWSER_CLIPBOARD_COMMANDS IDS_HTML_CLIPBOARD_OPERATION_DISABLED IDS_TE_TITLE IDS_TE_MESSAGE IDS_TE_INTERRUPT IDS_CMD_HELPTRAINING IDS_WEB_COMPAT_PLATFORM_UNSUPPORTED IDS_WEB_COMPAT_BROWSER_UNSUPPORTED IDS_WEB_COMPAT_BROWSER_VERSION_UNSUPPORTED IDS_WEB_COMPAT_WORK_IMPOSSIBLE IDS_WEB_BROWSERSLIST_HEADER IDS_WEB_BROWSERSLIST_DOWNLOAD IDS_WEB_BROWSERSLIST_VERSION_IE IDS_WEB_BROWSERSLIST_VERSION_FF IDS_WEB_BROWSERSLIST_VERSION_CHROME IDS_WEB_BROWSERSLIST_VERSION_SAFARI IDS_WEB_BROWSERSLIST_VERSION_SAFARI_IPAD IDS_WEB_BROWSER_UPDATE_LINK_IE IDS_WEB_BROWSER_UPDATE_LINK_SAFARI IDS_WEB_BROWSER_UPDATE_LINK_SAFARI_IPAD IDS_HTML_SET_WARN_EXT_COMP IDS_HTML_SET_GET_FILE IDS_HTML_SET_WARN_LINK_GET_FILE IDS_EXECUTE_NOT_SUPPORTED IDS_SEARCH_PROCESSING IDS_LOGOUT_WORK_FINISH IDS_WORK_IN_FULLSCREEN IDS_WORK_ONLY_IN_FULLSCREEN IDS_START_WORKING IDS_CONTINUE_WORK IDS_ECS_BROWSER_NOT_SUPPORTED IDS_ECS_MEDIA_REQUIRED_SECURE_ORIGIN IDS_ECS_MEDIA_NOT_AVAILABLE IDC_OIDC_STANDARD_LOGIN_NAME IDC_OIDC_ANOTHER_SERVICES IDC_OIDC_ERROR browsersettingsinfoieru.html browsersettingsinfoieen.html browsersettingsinfochru.html browsersettingsinfochen.html browsersettingsinfosfru.html browsersettingsinfosfen.html browsersettingsinfoff.html browsersettingsinfomain.html   Z a <a   Z i l s   K <kd a   I e i e t   R d +t   F a i l s   D z s t   I e l d e   I z l a s e   A t Fe m t   V s t u r e   S a r k a n s   L i c e n c e   S e r v i s s   S a m a z i n t   P i e l i e t o t   P i e v i e n o t   I e l d e . . .   P r t r a u k t   P a l i e l i n t   S k t   d a r b u   C i t a s   p o g a s   U z   t i k aa n o s !   F o n a   p i e m r s   F a i l a   i e l d e   L e j u p i e l d t   A t z +m t   v i s u   M e k l aa n a . . .   A t v r t   u z z i Fu   K r s a s   i z v l e   P a b e i g t   d a r b u   T u r p i n t   d a r b u   N o d a <a s   i z v l e   S a d a <u   p a n e l i s   M c +b u   v e r s i j a   T e k s t a   p i e m r s   R e s t a r t t   t a g a d   F a i l a   s a Fe m aa n a   I e i e t   a t k r t o t i   D a r b +b u   p a n e l i s   P i e v i e n o t   g r u p u   I z l a s e s   n o r d e s   I z v l t i e s   ( F 4 )   P i e v i e n o t   l a u k u s   K o p t   k   s k a i t l i   r j o   k o m p o n e n t u   U z d e v u m u   i z p i l d e   A t v r t   k a l e n d r u   S k t   u z s t d +aa n u   P r v i e t o t   u z   l e j u   A t t +r +t   ( S h i f t + F 4 )   I e l d t   k o m p o n e n t i   F o r m a s   i e s t a t +aa n a   P r v i e t o t   u z   a u g au   A t l i k t   u z s t d +aa n u   A t v r t   k a l k u l a t o r u   N a v i g c i j a s   p a n e l i s   A p m e k l t s   l a p p u s e s   '    #  ! !+  P a n e <a   a t i e s t a t +aa n a   " U z s t d +t   /   I n s t a l l "   K     i e s t a t +t   p r l kk u ?   A t v r t   ( C t r l + S h i f t + F 4 )   I z v l t i e s   n o   s a r a k s t a   O p e r c i j a   n a v   p i e e j a m a .   D a r b s   p i l n e k r n a   r e ~+m   V i d e o k a m e r a   n a v   p i e e j a m a   F o r m a s   e l e m e n t a   +p a a+b a s   D z s t   p a ar e i z j o   e l e m e n t u   P i e v i e n o t   v a i   d z s t   p o g a s   A t b a l s t m o   p r l kk u   s a r a k s t s   h t t p : / / w w w . a p p l e . c o m / r u / i o s /   I z p i l d +t   a t k r t o t u   p a l a i aa n u ?    r j s   k o m p o n e n t e s   u z s t d +aa n a .   R d +t   i n f o r m c i j u   p a r   p r o g r a m m u   p a p l a ai n j u m s   d a r b a m   a r   f a i l i e m   K o p t   a p m a i Fa s   b u f e r +  k   s k a i t l i   A t Fe m t   s k a i t l i   n o   a p m a i Fa s   b u f e r a   N o Fe m t   a t z +m i   n o   v i s i e m   e l e m e n t i e m   P i e v i e n o t   s k a i t l i   a p m a i Fa s   b u f e r i m   I z v l t i e s   n o   s a r a k s t a   ( C t r l + D o w n )   U z s t d +t   s t a n d a r t a   i e s t a t +j u m u s . . .   N o t i k u s i   s a v i e n o j u m a   p a g a i d u   k <km e .    r j   k o m p o n e n t e   v e i k s m +g i   u z s t d +t a .   p a p l a ai n j u m s   d a r b a m   a r   k r i p t o g r f i j u   P a p l a ai n j u m s   d a r b a m   a r   k r i p t o g r f i j u   I z v l i e t i e s   l a u k u s   i z v i e t o aa n a i   f o r m   P r v i e t o t   p a ar e i z j o   e l e m e n t u   u z   l e j u   P r v i e t o t   p a ar e i z j o   e l e m e n t u   u z   a u g au   N e v a r   i z p i l d +t   p r e j u   u z   g a l v e n o   l o g u .   D a r b s   i e s p j a m s   t i k a i   p i l n e k r n a   r e ~+m   " 1 C : U z Fm u m s   -   p a z i Fo j u m i   u n   p a l a i aa n a "   h t t p : / / w w w . a p p l e . c o m / r u / s a f a r i / d o w n l o a d /   ,�P a p l a ai n j u m a   u z s t d +aa n a   d a r b a m   a r   f a i l i e m .   ,�L a i   s k t u   u z s t d +aa n u   n o s p i e d i e t   " T u r p i n t " .   -�T i e k   i z p i l d +t a   r j s   k o m p o n e n t e s   u z s t d +aa n a   .�I e v a d i e t   1 C : U z Fm u m s   l i e t o t j a   v r d u   u n   p a r o l i   /�T i e k   u z s t d +t s   p a p a l a ai n j u m s   d a r b a m   a r   f a i l i e m   0�O p e r a t o r s   I z p i l d +t   w e b - k l i e n t   n e t i e k   a t b a l s t +t s   0�J a   n e i z d e v s   s a Fe m t   f a i l u ,   i z p i l d i e t   i e s t a t +j u m u   0�T i e k   i z p i l d +t a   r j s   k o m p o n e n t e s   u z s t d +aa n a . . .   1�D a r b a   a r   k r i p t o g r f i j u   p a p l a ai n j u m a   u z s t d +aa n a .   2�J ks u   p r l kk   n a v   a t <a t a   p i e k <u v e   a p m a i Fa s   b u f e r i m .   3�I z m a n t o j a m a i s   p r l kk s   n e a t b a l s t a   d a r b u   a r   1 C : D i a l o g   5�T i e k   u z s t d +t s   p a p a l a ai n j u m s   d a r b a m   a r   k r i p t o g r f i j u   7ܩ   S I A   " 1 !- S o f t " ,   1 9 9 6 . - 2 0 1 8 .   V i s a s   t i e s +b a s   a i z s a r g t a s   8�T i e k   v e i k t a   f o n a   u z d e v u m u   a p s t r d e . 
 L kd z u ,   u z g a i d i e t   . . .   9�I z p i l d +t a   p r e j a   u z   l o g u .   N o s p i e d i e t   L a b i ,   l a i   t u r p i n t u .   :�C O M - o b j e k t i   t i e k   a t b a l s t +t i   t i k a i   W i n d o w s   o p e r t j s i s t m s   <�*p a a+b u   n e v a r   m a i n +t   p c   #AB0=>2:00@0<5B@>2!50=A0  i z s a u k u m a   ?�P r o g r a m m a s   " 1 C : U z Fm u m s   -   p a z i Fo j u m i   u n   p a l a i aa n a "   u z s t d +aa n a .   A� r j   k o m p o n e n t e   v e i k s m +g i   u z s t d +t a   v a i   t   t i k a   u z s t d +t a   a g r k .   A�L a i   p i e k <kt u   v i d e o k a m e r a i ,   n e p i e c i e aa m s   d r o as   s a v i e n o j u m s   ( h t t p s )   C�A t b a l s t m s   v e r s i j a s :   5 2   u n   a u g s t k . < b r / > I e t e i c a m   v e r s i j a :   p d j .   C�A t b a l s t m s   v e r s i j a s :   1 0   u n   a u g s t k . < b r / > I e t e i c a m   v e r s i j a :   p d j .   D�A t b a l s t m s   v e r s i j a s :   4 . 0   u n   a u g s t k . < b r / > I e t e i c a m   v e r s i j a :   p d j .   E�h t t p : / / w i n d o w s . m i c r o s o f t . c o m / r u - R U / i n t e r n e t - e x p l o r e r / p r o d u c t s / i e / h o m e   F�A t b a l s t m s   v e r s i j a s :   4 . 0 . 5   u n   a u g s t k . < b r / > I e t e i c a m   v e r s i j a :   p d j .   M�T i e k   i z p i l d +t a   p r o g r a m m a s   " 1 C : U z Fm u m s   -   p a z i Fo j u m i   u n   p a l a i aa n a "   u z s t d +aa n a   O�J ks   v a r a t   i z m a n t o t   C t r l + C   k o p aa n a i ,   C t r l + X   i z g r i e aa n a i   u n   C t r l + V   i e v i e t o aa n a i .   P�A t b a l s t m s   v e r s i j a s :   4 . 0 . 4   ( i O S   3 . 2 )   u n   a u g s t k . < b r / > I e t e i c a m   v e r s i j a :   p d j .   R�L a i   a t t e i k t o s   n o   u z s t d +aa n a s ,   n o s p i e d i e t   " A t c e l t   /   D i s c a r d "   u n   a i z v e r i e t   ao   l o g u .   R� r j s   k o m p o n e n t e s   u z s t d +aa n a   n a v   i z p i l d +t a ! 
 U z s t d +aa n a s   p r o c e s   n o t i k u s i   k <kd a !   X�P a r d o t i e s   p i e p r a s +j u m a m   p a r   u z s t d +aa n a s   a t <a u j u ,   n o s p i e d i e t   p o g u   " U z s t d +t   /   I n s t a l l " .   _�< p > L a i   v e i k t u   % s   u z s t d +aa n u ,   n o s p i e d i e t   u z   s a Fe m t   f a i l a   u n   s a g a i d i e t   u z s t d +aa n a s   b e i g a s . < / p >   h�< p > L a i   v e i k t u   % s   u z s t d +aa n u ,   s a Fe m t o   f a i l u   p r v e i d o j i e t   p a r   i z p i l d f a i l u   u n   p a l a i d i e t   t o   u z   i z p i l d i . < / p >   u�P c   v e i k s m +g a s   p a p l a ai n j u m a   u z s t d +aa n a s   p r l kk a m   l a p p u s e   j r e s t a r t . 
 N o s p i e d i e t   " R e s t a r t t   t a g a d   " ,   l a i   r e s t a r t t u .   {�L a i   s k t u   u z s t d +aa n u ,   n o s p i e d i e t   " T l k   /   C o n t i n u e " .   P c   t a m   a p s t i p r i n i e t   p i e k r i aa n u ,   n o s p i e ~o t   p o g u   " U z s t d +t   /   I n s t a l l " .   ��V i e t n e   % h o s t n a m e %   m #i n a   v r s t i e s   p i e   a p m a i Fa s   b u f e r a .   N o s p i e d i e t   ' O K ' ,   l a i   a t <a u t u   aa i   v i e t n e i   p i e k <u v i .   N o p i e d i e t   ' A t c e l t ' ,   l a i   a i z l i e g t u   p i e k <u v i .   ��J ks u   i z m a n t o j a m   p r l kk a   v e r s i j a   n a v   1 C : U z Fm u m s   1 C : U z Fm u m s   a t b a l s t m o   v e r s i j u   s a s t v . 
 N o s p i e d i e t   L a b i ,   l a i   p r i e t u   p i e   a t b a l s t m o   p r l kk u   s a r a k s t a .   ��< p >   P c   v e i k s m +g a s   p r l kk a   p a p l a ai n j u m a   u n   k o m p o n e n t e s   u z s t d +aa n a s   l a p p u s e   j r e s t a r t .   L a i   v e i k t u   r e s t a r t aa n u ,   n o s p i e d i e t < b > " R e s t a r t t   t a g a d   " < / b > . < / p >   ��< p > P a p l a ai n j u m s   p r l kk a m   t i k a   i n s t a l t s .   A t l i c i s   u z s t d +t   % s . < / p > < p >   L a i   t o   v e i k t u ,   s a Fe m t o   f a i l u   p r v e i d o j i e t   p a r   i z p i l d f a i l u   u n   p a l a i d i e t   t o   u z   i z p i l d i . < / p > < p >   ��< p > P a p l a ai n j u m s   p r l kk a m   t i k a   i n s t a l t s .   A t l i c i s   u z s t d +t % s . < / p > < p > N o k l i k a7i n i e t   u z   s a Fe m t   f a i l a   ( p r l kk a   l o g a   a p a k aj   d a <)   u n   s a g a i d i e t   u z s t d +aa n a s   b e i g a s . < / p > < p >   ��L a i   p i e k <kt u   ai m   f u n k c i o n l a m ,   i e t e i c a m s   p a l a i s t   l i e t o t n i   p r l kk   I n t e r n e t   E x p l o r e r . 
 L a i   t o   v e i k t u ,   a t v e r i e t   p r l kk a   i z v l n i   a r   p o g u   " . . . "   u n   i z v l i e t i e s   p u n k t u   " A t v r t   i z m a n t o j o t   I n t e r n e t   E x p l o r e r " .   ��< p > L a i   i z p i l d +t u   d a r b +b u ,   j u z s t d a   % s . < / p > < p > L a i   s k t u   u z s t d +aa n u ,   n o s p i e d i e t   p o g u   < b > " T u r p i n t " < / b > . < / p > < p > T i k s   i z p i l d +t a   k o m p o n e n t e s   i e l d e .   P c   t a m   n o k l i k a7i n i e t   u z   s a Fe m t   f a i l a   u n   s a g a i d i e t   i e l d e s   b e i g a s . < / p >   ��< p > L a i   i z p i l d +t u   d a r b +b u ,   j u z s t d a   % s . < / p > < p > L a i   s k t u   u z s t d +aa n u ,   n o s p i e d i e t   p o g u   < b > " T u r p i n t " < / b > . < / p > < p > T i k s   i z p i l d +t a   k o m p o n e n t e s   i e l d e .   P c   t a m   s a Fe m t o   f a i l u   p a d a r i e t   p a r   i z p i l d f a i l u   u n   p a l a i d i e t   t o   u z   i z p i l d i . < / p >   ��J ks u   i z m a n t o j a m a i s   p r l kk s   n a v   1 C : U z Fm u m s   a t b a l s t m o   p r l kk u   s a r a k s t . 
 I e s p j a m a   n e k o r e k t a   s i s t m a s   d a r b +b a . 
 N o s p i e d i e t   L a b i   ( O K ) ,   l a i   t u r p i n t u   d a r b u . 
 N o s p i e d i e t   A t c e l t   ( C a n c e l ) ,   l a i   p r i e t u   p i e   a t b a l s t m o   p r l kk u   s a r a k s t a .   ��J ks u   i z m a n t o j a m   p r l kk a   v e r s i j a   n a v   1 C : U z Fm u m s   a t b a l s t m o   v e r s i j u   s a r a k s t . 
 I e s p j a m a   n e k o r e k t a   s i s t m a s   d a r b +b a . 
 N o s p i e d i e t   L a b i   ( O K ) ,   l a i   t u r p i n t u   d a r b u . 
 N o s p i e d i e t   A t c e l t   ( C a n c e l ) ,   l a i   p r i e t u   p i e   a t b a l s t m o   p r l kk u   s a r a k s t a .   ��K a d   p a r d s   p i e p r a s +j u m s   p a r   u z s t d +aa n u ,   p r l i e c i n i e t i e s   v a i   n o r d +t a i s   a u t o r s   i r   k o m p o n e n t e s   p i e g d t j s ,   k u r a m   J ks   u z t i c a t i e s .   L a i   a t t e i k t o s   n o   u z s t d +aa n a s ,   n o s p i e d i e t   " A t c e l t   /   C a n c e l " .   L a i   a p s t i p r i n t u   u z s t d +aa n u ,   n o s p i e d i e t   ��< p >   P a p l a ai n j u m s   p r l kk a m   t i k a   i n s t a l t s .   A t l i c i s   u z s t d +t   % s . < / p > < p > L a i   s k t u   u z s t d +aa n u ,   n o s p i e d i e t   p o g u   < b > " T u r p i n t " < / b > . < / p > < p > T i k s   i z p i l d +t a   k o m p o n e n t e s   i e l d e .   P c   t a m   n o k l i k a7i n i e t   u z   s a Fe m t   f a i l a   u n   s a g a i d i e t   i e l d e s   b e i g a s . < p >   ��< p >   P a p l a ai n j u m s   p r l kk a m   t i k a   i n s t a l t s .   A t l i c i s   u z s t d +t   % s . < / p > < p > L a i   s k t u   u z s t d +aa n u ,   n o s p i e d i e t   p o g u   < b > " T u r p i n t " < / b > . < / p > < p > T i k s   i z p i l d +t a   k o m p o n e n t e s   i e l d e .   P c   t a m   s a Fe m t o   f a i l u   p a d a r i e t   p a r   i z p i l d f a i l u   u n   p a l a i d i e t   t o   u z   i z p i l d i . < p >   �J ks u   i z m a n t o j a m   o p e r t j s i s t m a   n a v   1 C : U z Fm u m s   a t b a l s t m o   o p e r t j s i s t m u   s a r a k s t . 
 I e s p j a m a   n e k o r e k t a   s i s t m a s   d a r b +b a . 
 N o s p i e d i e t   L a b i   ( O K ) ,   l a i   t u r p i n t u   d a r b u . 
 N o s p i e d i e t   A t c e l t   ( C a n c e l ) ,   l a i   p r i e t u   p i e   a t b a l s t m o   p r l kk u   u n   o p e r t j s i s t m u   s a r a k s t a .   �L a i   p i l n v r t +g i   s t r d t u   a r   a p m a i Fa s   b u f e r i   p r l kk   F i e r f o x ,   j u z s t d a   p a p l a ai n j u m s . 
 L a i   u z s t d +t u   p a p l a ai n j u m u ,   n o s p i e d i e t   p o g u   " S k t   u z s t d +aa n u " .   K a d   p a r d +s i e s   z i Fo j u m s   p a r   t o ,   k a   F i r e f o x   i r   b l o 7j i s   u z s t d +aa n a s   p i e p r a s +j u m u ,   n o s p i e d i e t   p o g u   " A t <a u t " ,   p c   t a m   " U z s t d +t "   ��< p >   L a i   i z p i l d +t u   d a r b +b u ,   j u z s t d a   p a p l a ai n j u m s   p r l kk a m   u n   % s . < / p > < p > L a i   s k t u   u z s t d +aa n u ,   n o s p i e d i e t   p o g u   < b > " T u r p i n t " < / b > . < / p > < p > K a d   p a r d +s i e s   z i Fo j u m s   p a r   t o ,   k a   F i r e f o x   i r   b l o 7j i s   u z s t d +aa n a s   p i e p r a s +j u m u ,   n o s p i e d i e t   p o g u   " A t <a u t " ,   p c   t a m   " U z s t d +t " .   K a d   p a r d +s i e s   u z s t d +aa n a s   p i e p r a s +j u m s ,   p r l i e c i n i e t i e s   v a i   a u t o r s   i r   p a p l a ai n j u m a   p i e g d t j s ,   k u r a m   J ks   u z t i c a t i e s ! < / p >   ��L a i   p i l n v r t +g i   s t r d t u   a r   a p m a i Fa s   b u f e r i   p r l kk   C h r o m e ,   j u z s t d a   p r l kk a   p a p l a ai n j u m s . 
 L a i   s k t u   u z s t d +aa n u ,   n o s p i e d i e t   p o g u   " S k t   u z s t d +aa n u " ,   p c   t a m   n o s p i e d i e t   p o g u   " + 5A?;0B=>"   u z   a t v r t s   l a p a s   ( b ks   n e p i e c i e aa m a   l i e t o j u m p r o g r a m m a s   p r s t a r t aa n a ) . 
 L a i   a u t o m t i s k i   u z s t d +t u   p a p l a ai n j u m u ,   n k a m o   r e i z i   s t a r t j o t   l i e t o j u m p r o g r a m m u ,   n o s p i e d i e t   p o g u   " A t l i k t   u z s t d +aa n u " . 
 N o s p i e d i e t   " A t c e l t " ,   l a i   p a p l a ai n j u m u   n e u z s t d +t u   t a g a d .   ��< p > L a i   i z p i l d +t u   d a r b +b u ,   j u z s t d a   p a p l a ai n j u m s   p r l kk a m   u n   % s . < / p > < p > L a i   s k t u   u z s t d +aa n u ,   n o s p i e d i e t   p o g u   < b > " T u r p i n t " < / b > . < / p > < p > P r o g r a m m a   a t v r s   p a p l a ai n j u m a   l a p u   G o o g l e   C h r o m e   v e i k a l .   N o s p i e d i e t   p o g u   < b > " + & n b s p ; !"" < / b > ,   l a i   u z s t d +t u   p a p l a ai n j u m u . < / p > < p > P c   p a p l a ai n j u m a   u z s t d +aa n a s   j a i z v e r   p a p l a ai n j u m a   l a p a   u n   j u z s t d a   p a t i   k o m p o n e n t e . < / p > < p > L a i   t o   v e i k t u ,   s a Fe m t o   f a i l u   p a d a r i e t   p a r   i z p i l d f a i l u   u n   p a l a i d i e t   t o   u z   i z p i l d i . < / p >   ��N o k l i k a7i n i e t   u z   n o r d e s ,   k a s   a t r o d a s   a p a k a
 P r l kk s   i e l d s   u z s t d +aa n a s   f a i l u ,   k a s   s a t u r   p a p l a ai n j u m u . 
 J a   f a i l s   n e a t v r s i e s   a u t o m t i s k i ,   a t v e r i e t   t o   p a t s t v +g i ,   d i v r e i z   n o k l i k a7i n o t   u z   t   i e l d e s   l o g . 
 I z p i l d i e t   u z s t d +aa n u ,   s e k o j o t   i n s t r u k c i j m . 
 U z s t d +aa n a s   l a i k   J ks   v a r a t   p r b a u d +t   k o m p o n e n t e s   p i e g d t j u ,   n o k l i k a7i n o t   u z   i k o n a s ,   k a s   a t r o d a m a   u z s t d +aa n a s   l o g a   a u g aj   l a b a j   s t kr +.   P r l i e c i n i e t i e s ,   k a   n o r d +t s   p i e g d t j s ,   k u r a m   J ks   u z t i c a t i e s !   ��< p > L a i   i z p i l d +t u   d a r b +b u ,   j u z s t d a   p a p l a ai n j u m s   p r l kk a m   u n   % s . < / p > < p > L a i   s k t u   u z s t d +aa n u ,   n o s p i e d i e t   p o g u < b > " T u r p i n t " < / b > . < / p > < p > P r o g r a m m a   a t v r s   p a p l a ai n j u m a   l a p u   G o o g l e   C h r o m e   v e i k a l .   N o s p i e d i e t   p o g u   < b > " + & n b s p ; !"" < / b > ,   l a i   u z s t d +t u   p a p l a ai n j u m u .   < / p > < p > P c   p a p l a ai n j u m a   u z s t d +aa n a s   j a i z v e r   p a p l a ai n j u m a   l a p a   u n   j u z s t d a   p a t i   k o m p o n e n t e . < / p > < p > L a i   t o   v e i k t u ,   n o k l i k a7i n i e t   u z   s a Fe m t   f a i l a   ( p r l kk a   l o g a   a p a k aj   d a <)   u n   s a g a i d i e t   u z s t d +aa n a s   b e i g a s . < / p >   �����������
  ﻿<HTML xmlns="http://www.w3.org/1999/xhtml"><HEAD><TITLE>Kā iestatīt pārlūku</TITLE>
<META content="text/html; charset=utf-8" http-equiv=Content-Type>
<META content=IE=EmulateIE7 http-equiv=X-UA-Compatible>
<META content=no http-equiv=imagetoolbar><LINK rel="shortcut icon" href="e1csys/mngsrv/favicon.ico">
<STYLE type=text/css>BODY {
	OVERFLOW: hidden; HEIGHT: 100%; WIDTH: 100%; PADDING-BOTTOM: 0px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; PADDING-RIGHT: 0px
}
P {
	FONT-SIZE: 13pt; FONT-FAMILY: Times New Roman
}
IMG {
	MARGIN-BOTTOM: 48px
}
.title {
	FONT-SIZE: 21pt; FONT-FAMILY: Times New Roman
}
.step {
	PADDING-LEFT: 12px; BACKGROUND-COLOR: #99ccff
}
</STYLE>

<META name=GENERATOR content="MSHTML 11.00.9600.18921"></HEAD>
<BODY>
<DIV style="OVERFLOW: auto; HEIGHT: 100%; WIDTH: 100%">
<DIV style="PADDING-BOTTOM: 8px; PADDING-TOP: 8px; PADDING-LEFT: 8px; PADDING-RIGHT: 8px">
<P class=title style="FONT-WEIGHT: bold">Web pārlūka iestatīšana sistēmai 1С:Uzņēmums</P>
<P class="title step">1. solis </P>
<P>Nospiediet ikonu <B><I>Tools</I></B>, kas atrodas loga augšējā labajā sturī.</P>
<P>Izvēlnē izvēlieties punktu <B><I>Settings</I></B>.</P>
<DIV><IMG border=0 src="e1csys/mngsrv/img_ch_1_en.png?sysver=<%V8VER%>"></DIV>
<P>Atvērsies aizliknis&nbsp;<B><I>Settings-Basics</I></B>, uz kura tiks veikti visi turpmākie iestatījumi.</P>
<P>Lai atgrieztos pie instrukcijas, izvēlieties aizlikni <B><I>Kā iestatīt pārlūku</I></B>.</P>
<DIV><IMG border=0 src="e1csys/mngsrv/img_ch_0_en.png?sysver=<%V8VER%>"></DIV>
<P class="title step">2. solis </P>
<P>Pārejiet uz norādi&nbsp;<B><I>Show advanced settings...</I></B> ekrāna apakšējā daļā un nospiediet to.</P>
<DIV><IMG border=0 src="e1csys/mngsrv/img_ch_2_en.png?sysver=<%V8VER%>"></DIV>
<P>Pārejiet uz bloku <B><I>Downloads</I></B> un atzīmējiet karodziņu <B><I>Ask where to save each file before downloading</I></B>.</P>
<DIV><IMG border=0 src="e1csys/mngsrv/img_ch_3_en.png?sysver=<%V8VER%>"></DIV>
<P class="title step">3. solis </P>
<P>Kad ir atzīmēts karodziņš, nospiediet pogu <B><I>Content Settings</I></B>, kura atrodas augstāk - blokā <B><I>Privacy</I></B>.</P>
<DIV><IMG border=0 src="e1csys/mngsrv/img_ch_4_en.png?sysver=<%V8VER%>"></DIV>
<P>Atvērtajā logā <B><I>Content Settings</I></B> pārejiet uz bloku <B><I>Pop-Ups</I></B> un atzīmējiet <B><I>Allow all sites to show pop-ups</I></B>.</P>
<DIV><IMG border=0 src="e1csys/mngsrv/img_ch_5_en.png?sysver=<%V8VER%>"></DIV>
<P style="BACKGROUND-COLOR: #e4e4e4">Pievērsiet uzmanību tam vai jūsu pārlūkā nav jāsaglabā izmaiņas.&nbsp;Lai pabeigtu iestatīšanu, &nbsp;pietiekami aizvērt aizlikni&nbsp;<I>Settings</I>.</P></DIV></DIV></BODY></HTML>��  ﻿<HTML xmlns="http://www.w3.org/1999/xhtml"><HEAD><TITLE><%IDS_HTML_BROWSER_SETTINGS%></TITLE>
<META content="text/html; charset=utf-8" http-equiv=Content-Type>
<META content=IE=EmulateIE7 http-equiv=X-UA-Compatible>
<META content=no http-equiv=imagetoolbar>
<STYLE type=text/css>BODY {
	PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 0px; WIDTH: 100%; PADDING-RIGHT: 0px; HEIGHT: 100%; OVERFLOW: hidden; PADDING-TOP: 0px
}
P {
	FONT-FAMILY: Times New Roman; FONT-SIZE: 13pt
}
IMG {
	MARGIN-BOTTOM: 48px
}
.title {
	FONT-FAMILY: Times New Roman; FONT-SIZE: 21pt
}
.step {
	BACKGROUND-COLOR: #99ccff; PADDING-LEFT: 12px
}
</STYLE>

<META name=GENERATOR content="MSHTML 9.00.8112.16440"></HEAD>
<BODY>
<DIV style="WIDTH: 100%; HEIGHT: 100%; OVERFLOW: auto">
<DIV style="PADDING-BOTTOM: 8px; PADDING-LEFT: 8px; PADDING-RIGHT: 8px; PADDING-TOP: 8px">
<P style="FONT-WEIGHT: bold" class=title>Web pārlūka iestatīšana sistēmai 1С:Uzņēmumam</P>
<P class="title step">1 solis </P>
<P>Nospiediet ikonu <B><I>Инструменты</I></B>, kas atrodas loga augšējā labajā sturī.</P>
<P>Izvēlnē izvēlieties punktu&nbsp; <B><I>Настройки</I></B>.</P>
<DIV><IMG border=0 src="file:///P:/8.3/res_ru/MNGSRV/src/res_lv/img_ch_1_ru.png?sysver=<%V8VER%>"></DIV>
<P>Atvērsies aizliknis <B><I>Настройки-Основные</I></B>, uz kura tiks veikti visi turpmākie iestatījumi.</P>
<P>Lai atgrieztos pie instrukcijas, izvēlieties aizlikni <B><I>Как настроить браузер</I></B>.</P>
<DIV><IMG border=0 src="file:///P:/8.3/res_ru/MNGSRV/src/res_lv/img_ch_0_ru.png?sysver=<%V8VER%>"></DIV>
<DIV>&nbsp;</DIV>
<P class="title step">2 solis </P>
<P>Pārejiet uz norādi <B><I>Показать дополнительные настройки</I></B> ekrāna apakšējā daļā un nospiediet to.</P>
<DIV><IMG border=0 src="img_ch_2_ru.png?sysver=<%V8VER%>"></DIV>
<P>Pārejiet uz bloku <B><I>Загрузки</I></B> un atzīmējiet karodziņu <B><I>Запрашивать место для сохранения каждого файла перед загрузкой</I></B>.</P>
<DIV><IMG border=0 src="img_ch_3_ru.png?sysver=<%V8VER%>"></DIV>
<P class="title step">3 solis </P>
<P>Kad ir atzīmēts karodziņš, nospiediet pogu <B><I>Настройки контента</I></B>, kura atrodas augstāk - blokā <B><I>Личные данные</I></B>.</P>
<DIV><IMG border=0 src="img_ch_4_ru.png?sysver=<%V8VER%>"></DIV>
<P>Atvērtajā logā <B><I>Настройки контента</I></B> pārejiet uz bloku <B><I>Всплывающие окна</I></B> un atzīmējiet <B><I>Разрешить открытие всплывающих окон на всех сайтах</I></B>.</P>
<DIV><IMG border=0 src="img_ch_5_ru.png?sysver=<%V8VER%>"></DIV>
<P style="BACKGROUND-COLOR: #e4e4e4">Pievērsiet uzmanību tam vai jūsu pārlūkā nav jāsaglabā izmaiņas. Lai pabeigtu iestatīšanu, pietiekami aizvērt aizlikni <I>Настройки</I>.</P></DIV></DIV></BODY></HTML>
  ﻿<HTML xmlns="http://www.w3.org/1999/xhtml"><HEAD><TITLE>Kā iestatīt pārlūku</TITLE>
<META content="text/html; charset=utf-8" http-equiv=Content-Type>
<META content=IE=EmulateIE7 http-equiv=X-UA-Compatible>
<META content=no http-equiv=imagetoolbar><LINK rel="shortcut icon" href="e1csys/mngsrv/favicon.ico">
<STYLE type=text/css>BODY {
	OVERFLOW: hidden; HEIGHT: 100%; WIDTH: 100%; PADDING-BOTTOM: 0px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; PADDING-RIGHT: 0px
}
P {
	FONT-SIZE: 13pt; FONT-FAMILY: Times New Roman
}
IMG {
	MARGIN-BOTTOM: 48px
}
.title {
	FONT-SIZE: 21pt; FONT-FAMILY: Times New Roman
}
.step {
	PADDING-LEFT: 12px; BACKGROUND-COLOR: #99ccff
}
</STYLE>

<META name=GENERATOR content="MSHTML 11.00.9600.18921"></HEAD>
<BODY>
<DIV style="OVERFLOW: auto; HEIGHT: 100%; WIDTH: 100%">
<DIV style="PADDING-BOTTOM: 8px; PADDING-TOP: 8px; PADDING-LEFT: 8px; PADDING-RIGHT: 8px">
<P class=title style="FONT-WEIGHT: bold">Web pārlūka iestatīšana sistēmai 1C:Uzņēmums </P>
<UL>
<LI>Lai lietojumprogrammā atļautu uznirstošos logus, pārlūka izvēlnē <B>Tools</B> izvēlieties punktu <B>Options</B>;<BR>Atvērtā logā pārejiet uz sadaļu <B>Content</B>;<BR>Noņemiet karodziņu&nbsp;<B>Block pop-up windows</B>. 
<LI>Lai manuāli atļautu pārslēgties starp lietojumprogrammas logiem, pārlūka&nbsp;adreses rindā ievadiet <B>about:config</B>;<BR>Pēc tam filtra rindā ievadiet <CODE>dom.disable_window_flip</CODE>;<BR>Nomainiet šī iestatījuma nozīmi uz&nbsp;<CODE>false</CODE>. 
<LI>Lai manuāli atļautu izmantot palaišanas rindas parametros nelatīņu simbolus,&nbsp;pārlūka&nbsp;adreses rindā ievadiet <B>about:config</B>;<BR>Pēc tam filtra rindā ievadiet <CODE>network.standard-url.encode-query-utf8</CODE>. Ja šis iestatījums netiks atrasts, ievadiet <CODE>browser.fixup.use-utf8</CODE>;<BR>Nomainiet šī iestatījuma nozīmi uz&nbsp;<CODE>true</CODE>. 
<LI>Lai manuāli atļautu izmantot tstatūru, lai pārslēgtos starp lietojumprogrammas logiem, pārlūka&nbsp;adreses rindā ievadiet <B>about:config</B>;<BR>Pēc tam filtra rindā ievadiet <CODE>dom.popup_allowed_events</CODE>;<BR>Pievienojiet šī iestatījuma nozīmei notikumu <B>keydown</B>. 
<LI>Lai manuāli iestatītu operētājsistēmas autentifikāciju, pārlūka&nbsp;adreses rindā ievadiet <STRONG>about:config</STRONG>;<BR>Pēc tam iestatījumu lapas filtra rindā ievadiet parametra nosaukumu;<BR>Šis iestatījums tiek piemērots trim parametriem: <CODE>network.automatic-ntlm-auth.trusted-uris</CODE>, <CODE>network.negotiate-auth.delegation-uris</CODE>, <CODE>network.negotiate-auth.trusted-uris</CODE>;<BR>Tālāk uzdodiet web serveru sarakstu, no kuriem tiks strādāts ar "1C:Uzņēmums" bāzi. </LI></UL>
<P>Iestatīšana pabeigta.</P></DIV></DIV></BODY></HTML>���	  ﻿<HTML xmlns="http://www.w3.org/1999/xhtml"><HEAD><TITLE>Kā iestatīt pārlūku</TITLE>
<META content="text/html; charset=utf-8" http-equiv=Content-Type>
<META content=IE=EmulateIE7 http-equiv=X-UA-Compatible>
<META content=no http-equiv=imagetoolbar><LINK rel="shortcut icon" href="e1csys/mngsrv/favicon.ico">
<STYLE type=text/css>BODY {
	OVERFLOW: hidden; HEIGHT: 100%; WIDTH: 100%; PADDING-BOTTOM: 0px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; PADDING-RIGHT: 0px
}
P {
	FONT-SIZE: 13pt; FONT-FAMILY: Times New Roman
}
IMG {
	MARGIN-BOTTOM: 48px
}
.title {
	FONT-SIZE: 21pt; FONT-FAMILY: Times New Roman
}
.step {
	PADDING-LEFT: 12px; BACKGROUND-COLOR: #99ccff
}
</STYLE>

<META name=GENERATOR content="MSHTML 11.00.9600.18921"></HEAD>
<BODY>
<DIV style="OVERFLOW: auto; HEIGHT: 100%; WIDTH: 100%">
<DIV style="PADDING-BOTTOM: 8px; PADDING-TOP: 8px; PADDING-LEFT: 8px; PADDING-RIGHT: 8px">
<P class=title style="FONT-WEIGHT: bold">Web pārlūka iestatīšana sistēmai 1С:Uzņēmums</P>
<P class="title step">1. solis</P>
<P>Ar peles labo taustiņu nospiediet jebkurā brīvā vietā zem adrešu rindas (attēlā atzīmēta gaiši sarkanā krāsā) un izvēlnē izvēlieties punktu <B><I>Menu Bar</I></B>.</P>
<DIV><IMG border=0 src="e1csys/mngsrv/img_ie_0_en.png?sysver=<%V8VER%>"></DIV>
<DIV><IMG border=0 src="e1csys/mngsrv/img_ie_01_en.png?sysver=<%V8VER%>"></DIV>
<P>Zem adrešu rindas parādīsies izvēlne. Atrodiet punktu <B><I>Tools</I></B>.</P>
<DIV><IMG border=0 src="e1csys/mngsrv/img_ie_1_en.png?sysver=<%V8VER%>"></DIV>
<P>Nospiediet to, atvērsies izvēlne. Izvēlieties punktu <B><I>Internet Options</I></B>.</P>
<DIV><IMG border=0 src="e1csys/mngsrv/img_ie_2_en.png?sysver=<%V8VER%>"></DIV>
<P class="title step">2. solis</P>
<P>Atvērtajā logā pārejiet uz aizliktni <B><I>Privacy</I></B>.</P>
<DIV><IMG border=0 src="e1csys/mngsrv/img_ie_3_en.png?sysver=<%V8VER%>"></DIV>
<P>Loga apakšējā daļā atrodiet un <B><I>noņemiet </I></B>karodziņu <B><I>Turn on Pop-up Blocker</I></B>.</P>
<DIV><IMG border=0 src="e1csys/mngsrv/img_ie_4_en.png?sysver=<%V8VER%>"></DIV>
<P>Kad karodziņš tiks noņemts, nospiediet pogu <B><I>ОК</I></B>, lai saglabātu iestatījumus.</P>
<DIV><IMG border=0 src="e1csys/mngsrv/img_ie_5_en.png?sysver=<%V8VER%>"></DIV>
<P>Iestatīšana pabeigta.</P>
<P style="FONT-SIZE: 9pt">Lai slēptu izvēlni zem adrešu rindas, atkārtojiet 1 soļa pirmo daļu.</P></DIV></DIV></BODY></HTML>���B
  ﻿<HTML xmlns="http://www.w3.org/1999/xhtml"><HEAD><TITLE>Kā iestatīt pārlūku</TITLE>
<META content="text/html; charset=utf-8" http-equiv=Content-Type>
<META content=IE=EmulateIE7 http-equiv=X-UA-Compatible>
<META content=no http-equiv=imagetoolbar><LINK rel="shortcut icon" href="e1csys/mngsrv/favicon.ico">
<STYLE type=text/css>BODY {
	OVERFLOW: hidden; HEIGHT: 100%; WIDTH: 100%; PADDING-BOTTOM: 0px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; PADDING-RIGHT: 0px
}
P {
	FONT-SIZE: 13pt; FONT-FAMILY: Times New Roman
}
IMG {
	MARGIN-BOTTOM: 48px
}
.title {
	FONT-SIZE: 21pt; FONT-FAMILY: Times New Roman
}
.step {
	PADDING-LEFT: 12px; BACKGROUND-COLOR: #99ccff
}
</STYLE>

<META name=GENERATOR content="MSHTML 11.00.9600.18921"></HEAD>
<BODY>
<DIV style="OVERFLOW: auto; HEIGHT: 100%; WIDTH: 100%">
<DIV style="PADDING-BOTTOM: 8px; PADDING-TOP: 8px; PADDING-LEFT: 8px; PADDING-RIGHT: 8px">
<P class=title style="FONT-WEIGHT: bold">Web pārlūka iestatīšana sistēmai 1С:Uzņēmums</P>
<P class="title step">1. solis</P>
<P>Ar peles labo taustiņu nospiediet jebkurā brīvā vietā zem adrešu rindas (attēlā atzīmēta gaiši sarkanā krāsā) un izvēlnē izvēlieties punktu&nbsp; <B><I>Строка меню</I></B>.</P>
<DIV><IMG border=0 src="e1csys/mngsrv/img_ie_0_ru.png?sysver=<%V8VER%>"></DIV>
<DIV><IMG border=0 src="e1csys/mngsrv/img_ie_01_ru.png?sysver=<%V8VER%>"></DIV>
<P>Zem adrešu rindas parādīsies izvēlne. Atrodiet punktu <B><I>Сервис</I></B>.</P>
<DIV><IMG border=0 src="e1csys/mngsrv/img_ie_1_ru.png?sysver=<%V8VER%>"></DIV>
<P>Nospiediet to, atvērsies izvēlne. Izvēlieties punktu <B><I>Свойства обозревателя</I></B>.</P>
<DIV><IMG border=0 src="e1csys/mngsrv/img_ie_2_ru.png?sysver=<%V8VER%>"></DIV>
<P class="title step">2. solis</P>
<P>Atvērtajā logā pārejiet uz aizliktni&nbsp; <B><I>Конфиденциальность</I></B>.</P>
<DIV><IMG border=0 src="e1csys/mngsrv/img_ie_3_ru.png?sysver=<%V8VER%>"></DIV>
<P>Loga apakšējā daļā atrodiet un <B><I>noņemiet </I></B>karodziņu <B><I>Включить блокирование всплывающих окон</I></B>.</P>
<DIV><IMG border=0 src="e1csys/mngsrv/img_ie_4_ru.png?sysver=<%V8VER%>"></DIV>
<P>Kad karodziņš tiks noņemts, nospiediet pogu <B><I>ОК</I></B>, lai saglabātu iestatījumus.</P>
<DIV><IMG border=0 src="e1csys/mngsrv/img_ie_5_ru.png?sysver=<%V8VER%>"></DIV>
<P>Iestatīšana pabeigta.</P>
<P style="FONT-SIZE: 9pt">Lai slēptu izvēlni zem adrešu rindas, atkārtojiet 1 soļa pirmo daļu.</P></DIV></DIV></BODY></HTML>����������c  ﻿<html class="IWeb"><head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Pārlūka iestatīšana</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="imagetoolbar" content="no">
        <link href="e1csys/mngsrv/favicon.ico" rel="shortcut icon">
        <style type="text/css">
        .main
        {
            width:720px;
            margin:0 auto;
        }
        .head
        {
            font-size:32pt;
            font-family:Times New Roman;
        }
        .title
        {
            font-size:20pt;
            font-family:Times New Roman;
        }
        .txt
        {
            font-size:14pt;
            font-family:Times New Roman;
        }
        </style>
<meta name="GENERATOR" content="MSHTML 11.00.9600.18921"></head>
<body style="margin: 0px; padding: 0px; width: 100%; height: 100%; overflow: hidden;">
<div style="width: 100%; height: 100%; -ms-overflow-x: hidden; -ms-overflow-y: auto;">
<div style="padding: 10px 30px;">
<p><span class="txt"><b>Jauna loga atvēršana tika bloķēta, iespējams nostrādājis uznirstošo logu bloķētājs.</b></span></p>
<p><span class="txt">Lai turpinātu darbu, izpildiet web pārlūka iestatīšanu.<br>
Lai saņemtu iestatīšanas instrukciju, nospiediet pogu <b><i>Atvērt instrukciju</i></b>.<br>
</strong>Pēc iestatīšanas nospiediet pogu <b><i>Labi</i></b> un veiciet atkārtotu palaišanu.</span></p>
<div align="center" style="width: 100%; height: 50px; padding-top: 20px;">
    <div style="width: 450px; height: 30px; position: relative;">
    <button class="webButton" id="okButton" style="left: 10px;">Atvērt instrukciju</button>
    <button class="webButton" id="cancelButton" style="left: 230px;">Labi</button>
    </div>
    </div>
</div>
</div>
<!--@remove+@-->
<script type="text/javascript">
    var CLOSURE_NO_DEPS = true;
    var CLOSURE_BASE_PATH = "scripts/";
</script>
<script src="scripts/webtools/libs/closure-library/closure/goog/base.js?sysver=<%V8VER%>" type="text/javascript"></script></body></html>���������  ﻿<HTML xmlns="http://www.w3.org/1999/xhtml"><HEAD><TITLE><%IDS_HTML_BROWSER_SETTINGS%></TITLE>
<META content="text/html; charset=utf-8" http-equiv=Content-Type>
<META content=IE=EmulateIE7 http-equiv=X-UA-Compatible>
<META content=no http-equiv=imagetoolbar>
<STYLE type=text/css>BODY {
	PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 0px; WIDTH: 100%; PADDING-RIGHT: 0px; HEIGHT: 100%; OVERFLOW: hidden; PADDING-TOP: 0px
}
P {
	FONT-FAMILY: Times New Roman; FONT-SIZE: 13pt
}
IMG {
	MARGIN-BOTTOM: 48px
}
.title {
	FONT-FAMILY: Times New Roman; FONT-SIZE: 21pt
}
.step {
	BACKGROUND-COLOR: #99ccff; PADDING-LEFT: 12px
}
</STYLE>

<META name=GENERATOR content="MSHTML 9.00.8112.16440"></HEAD>
<BODY>
<DIV style="WIDTH: 100%; HEIGHT: 100%; OVERFLOW: auto">
<DIV style="PADDING-BOTTOM: 8px; PADDING-LEFT: 8px; PADDING-RIGHT: 8px; PADDING-TOP: 8px">
<P style="FONT-WEIGHT: bold" class=title>Web pārlūka iestatīšana sistēmai 1С:Uzņēmums</P>
<P>Augšējā kreisajā stūrī atrodiet izvēlnes punktu <B><I>Safari</I></B>, nospiediet to. Atvērtajā izvēlnē noņemiet karodziņu <B><I>Block Pop-Up Windows</I></B>.</P>
<DIV><IMG border=0 src="e1csys/mngsrv/img_sf_en.png?sysver=<%V8VER%>"></DIV>
<P>Iestatīšana pabeigta.</P></DIV></DIV></BODY></HTML>�����  ﻿<HTML xmlns="http://www.w3.org/1999/xhtml"><HEAD><TITLE><%IDS_HTML_BROWSER_SETTINGS%></TITLE>
<META content="text/html; charset=utf-8" http-equiv=Content-Type>
<META content=IE=EmulateIE7 http-equiv=X-UA-Compatible>
<META content=no http-equiv=imagetoolbar>
<STYLE type=text/css>BODY {
	PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 0px; WIDTH: 100%; PADDING-RIGHT: 0px; HEIGHT: 100%; OVERFLOW: hidden; PADDING-TOP: 0px
}
P {
	FONT-FAMILY: Times New Roman; FONT-SIZE: 13pt
}
IMG {
	MARGIN-BOTTOM: 48px
}
.title {
	FONT-FAMILY: Times New Roman; FONT-SIZE: 21pt
}
.step {
	BACKGROUND-COLOR: #99ccff; PADDING-LEFT: 12px
}
</STYLE>

<META name=GENERATOR content="MSHTML 8.00.6001.23536"></HEAD>
<BODY>
<DIV style="WIDTH: 100%; HEIGHT: 100%; OVERFLOW: auto">
<DIV style="PADDING-BOTTOM: 8px; PADDING-LEFT: 8px; PADDING-RIGHT: 8px; PADDING-TOP: 8px">
<P style="FONT-WEIGHT: bold" class=title>Web pārlūka iestatīšana sistēmai 1С:Uzņēmums</P>
<P>Augšējā kreisajā stūrī atrodiet izvēlnes punktu <B><I>Safari</I></B>, nospiediet to. Atvērtajā izvēlnē noņemiet karodziņu <B><I>Блокировать всплывающие окна</I></B>.</P>
<DIV><IMG border=0 src="img_sf_ru.png?sysver=<%V8VER%>"></DIV>
<P>Iestatīšana pabeigta.</P></DIV></DIV></BODY></HTML>�� ��� �#��<�\5�X�<���u�����o3t���^F � � _ � z � ��d?_E2��
���N	�D��~�G`�Y5
V
���r
�	�
z
�0	�$
�
v	��v���	�	0I�	�d
	Z�����	�
��	��
���
�9Ll%E��  ]�wj$Y=��w��(Gj���*A}`�C&���� `  `  `g `� `�
 ` `� `: `� `� `w ` `` `Q `� `u `� `7 `�# `*  `� `r `� ` `� `� `W `| `* `� ` `]	 `(	 `� `@ `� `] `^  `c `P `L `� `�  `+ `� `� `� `� `� `� `? `� `� `- `� `h  `�  `  `I `, `� `z `7  `r  `� ` `�	 `� `#  `� `� `�  `�
 `� `( `� `0  `� `  `� `�  `� `� `  `>  `� `|  `�  `R `x `K `� `� `� `� `) `  `F  `� `s `N  `V  `� `6 `] `� `  `� `f `H `�  `9 `� `
 `)  `1 `
 `! ` `m ` `B
 `� `p `�! `�  `~
 `�  `�  `�	 ` `� `�  ` ` `� `� `� `� `S ` `, `2 ` `� `, `� `� ` � � � �! �$ �& �' 